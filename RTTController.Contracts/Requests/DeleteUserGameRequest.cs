﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
   public class DeleteUserGameRequest
    {
        public int UserID { get; set; }

        public int GameID { get; set; }
    }
}
