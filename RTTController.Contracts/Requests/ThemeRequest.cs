﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class ThemeRequest
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
