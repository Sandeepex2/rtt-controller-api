﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class ValueRequest
    {
        public int GameId { get; set; }
        public int CategoryId { get; set; }
        public string SubCategory { get; set; }
        public string EventCode { get; set; }
        public string LayerName { get; set; }
    }
}
