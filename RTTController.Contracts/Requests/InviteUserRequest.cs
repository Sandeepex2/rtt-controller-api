﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class InviteUserRequest
    {
        public string Email { get; set; }

        public int GameId { get; set; }

        public Guid EmailToken { get; set; }
    }
}
