﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class GameLeadRequest
    {
        public int GameId { get; set; }

        public int UserId { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zipcode { get; set; }

        public string Phone { get; set; }

        public string PhotoType { get; set; }

        public string Photo { get; set; }

        public List<ReferralSource> ReferralSources { get; set; }

    }

    public class ReferralSource
    {
        public int ReferType { get; set; }

        public int ReferValue { get; set; }

        public string Description { get; set; }
    }
}
