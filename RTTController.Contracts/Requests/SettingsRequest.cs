﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{    
    public class SettingsRequest
    {   
        public int Id { get; set; }                   
        public string Name { get; set; }

        public List<SettingOptions> Options { get; set; }
    }

    public class SettingOptions
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SettingsId { get; set; }
        
    }
}
