﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class CategoryRequest
    {
        public string EventCode { get; set; }

        public string Name { get; set; }

        public string CreatedBy { get; set; }

    }
}
