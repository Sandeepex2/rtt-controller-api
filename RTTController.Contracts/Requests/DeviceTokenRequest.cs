﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class DeviceTokenRequest
    {
        public int UserId { get; set; }
        public string DeviceToken { get; set; }
    }
}
