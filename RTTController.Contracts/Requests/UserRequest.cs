﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class UserRequest
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public int UserType { get; set; }

        public string Password { get; set; }

        public int GameId { get; set; }

        public string InviteToken { get; set; }
    }
}
