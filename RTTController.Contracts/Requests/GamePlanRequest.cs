﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class GamePlanRequest
    {
        public int GameId { get; set; }

        public int BuilderId { get; set; }

        public string GameName { get; set; }
        
        public string BDXCommunityId { get; set; }

        public List<GameFloorImage> GamePlanImages { get; set; }

        public CategoryGameMapping CategoryGameMap { get; set; }
    }
    public class GameFloorImage
    {
        public int FloorId { get; set; }

        public string FloorImage { get; set; }
    }

    public class CategoryGameMapping
    {
        public int GameId { get; set; }

        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }

        public int ThemeId { get; set; }

        public List<GamePlanCategoryEvent> GamePlanCategoryEvents { get; set; }
    }

    public class GamePlanCategoryEvent
    {
        public string EventCode { get; set; }
        public string ImageUrl { get; set; }
        public string LayerName { get; set; }
    }
}
