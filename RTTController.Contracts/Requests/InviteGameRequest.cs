﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class InviteGameRequest
    {
        public int GameId { get; set; }

        public string GameName { get; set; }

        public string Email { get; set; }
    }
}
