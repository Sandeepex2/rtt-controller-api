﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class EmailRequest
    {
        public string Recipient { get; set; }

        public string Sender { get; set; }

        public string Subject { get; set; }

        public string HtmlBody { get; set; }
    }
}
