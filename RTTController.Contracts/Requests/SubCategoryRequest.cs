﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class SubCategoryRequest
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }

        public int GameId { get; set; }

        public int? ThemeId { get; set; }

        public int Sequence { get; set; }
    }

    public class SubCategoryUpdateRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class SubCategorySequecneUpdateRequest
    {        
        public List<string> sequenceArray { get; set; }
    }


}
