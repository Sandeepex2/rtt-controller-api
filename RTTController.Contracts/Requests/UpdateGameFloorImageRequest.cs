﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
   public class UpdateGameFloorImageRequest
    {
        public int GameID { get; set; }

        public int GameFloorID { get; set; }

        public string ImageName { get; set; }
    }


    public class AddGameFloorImageRequest
    {
        public int GameID { get; set; }

        public List<GameFloorImage> GamePlanImages { get; set; }
    }
}
