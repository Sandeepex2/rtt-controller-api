﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class LeadRequest
    {
        public int? UserId { get; set; }

        public int? GameId { get; set; }
    }
}
