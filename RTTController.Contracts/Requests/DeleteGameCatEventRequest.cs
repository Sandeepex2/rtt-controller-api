﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Requests
{
    public class DeleteGameCatEventRequest
    {
        public int GameId { get; set; }

        public int EventId { get; set; }
    }
}
