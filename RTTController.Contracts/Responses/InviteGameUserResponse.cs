﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("InviteGameUserResponse")]
    public class InviteGameUserResponse
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("gameId")]
        public int GameID { get; set; }

        [JsonProperty("gameName")]
        public string GameName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("inviteDate")]
        public string  InviteDate { get; set; }
    }
}
