﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("gameLeadResponse")]
    public class GameLeadResponse
    {
        [JsonProperty("gameId")]
        public int GameId { get; set; }

        [JsonProperty("gameName")]
        public string GameName { get; set; }

        [JsonProperty("leads")]
        public List<GameUserLead> GameUserLead { get; set; }

        public GameLeadResponse()
        {
            GameUserLead = new List<GameUserLead>();
        }
    }

    [JsonObject("leads")]
    public class GameUserLead
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("referUser")]
        public string ReferUser { get; set; }

        [JsonProperty("gameId")]
        public int GameId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("zipCode")]
        public string Zipcode { get; set; }

        [JsonProperty("phone")]
        public string PhoneNumber { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }       


        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("createdDate")]
        public DateTime? CreatedDate { get; set; }

        [JsonProperty("referralSources")]
        public List<LeadReferralSource> ReferralSources { get; set; }

    }

    [JsonObject("leadReferralSource")]
    public class LeadReferralSource
    {
        [JsonProperty("type")]
        public string ReferType { get; set; }

        public List<ReferrerValue> ReferrerValues { get; set; }
    }

    [JsonObject("referrerValue")]
    public class ReferrerValue
    {
        [JsonProperty("name")]
        public string ReferValue { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
