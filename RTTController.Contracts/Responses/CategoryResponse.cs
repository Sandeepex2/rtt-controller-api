﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("Category")]
    public class CategoryResponse
    {
        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("eventCode")]
        public string EventCode { get; set; }

        [JsonProperty("categoryName")]
        public string Name { get; set; }

        [JsonProperty("subCategories")]
        public List<SubCategory> SubCategories { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonObject("subCategory")]
        public class SubCategory
        {
            [JsonProperty("subCategoryId")]
            public int SubCategoryId { get; set; }

            [JsonProperty("subCategoryName")]
            public string Name { get; set; }
        }
    }
}
