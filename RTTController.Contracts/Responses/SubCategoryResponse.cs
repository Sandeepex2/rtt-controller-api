﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("subCategory")]
    public class SubCategoryResponse
    {
        [JsonProperty("subCategoryId")]
        public int Id { get; set; }

        [JsonProperty("subCategoryName")]
        public string Name { get; set; }
    }
}
