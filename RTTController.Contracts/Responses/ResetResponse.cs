﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace RTTController.Contracts.Responses
{
    public class ResetResponse
    {
        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("isVerified")]
        public bool IsVerified { get; set; }
    }
}
