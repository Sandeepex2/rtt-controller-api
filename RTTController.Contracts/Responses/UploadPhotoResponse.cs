﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace RTTController.Contracts.Responses
{
    public class UploadPhotoResponse
    {
        public HttpStatusCode Status { get; set; }

        public string FileName { get; set; }
    }
}
