﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    public class BaseApiResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public int TotalRecords { get; set; }
        public object Result { get; set; }
    }
}
