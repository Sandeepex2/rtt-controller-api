﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("inviteUser")]
    public class InviteUserResponse
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("emailToken")]
        public string EmailToken { get; set; }

        [JsonProperty("gameId")]
        public int GameId { get; set; }

        [JsonProperty("inviteAccept")]
        public bool InviteAccept { get; set; }
    }
}
