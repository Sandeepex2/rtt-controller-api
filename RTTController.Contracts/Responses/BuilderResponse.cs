﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("builder")]
    public class BuilderResponse
    {
        [JsonProperty("builderId")]
        public int BuilderId { get; set; }

        [JsonProperty("builderName")]
        public string BuilderName { get; set; }

        [JsonProperty("bdxPartnerId")]
        public string BDXPartnerId { get; set; }

        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }

        [JsonProperty("logoUrl")]
        public string LogoUrl { get; set; }

        [JsonProperty("gamePlans")]
        public List<GamePlanResponse> GamePlans { get; set; }
    }
}
