﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace RTTController.Contracts.Responses
{
    [JsonObject("authenticate")]
    public class AuthenticateResponse
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("userType")]
        public string UserType { get; set; }

        [JsonProperty("expires")]
        public string Expired { get; set; }
    }
}
