﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("theme")]
    public class ThemeResponse
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("gameId")]
        public int? GameId { get; set; }

        [JsonProperty("categoryId")]
        public int? CategoryId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
