﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("gamePlan")]
    public class GamePlanResponse
    {
        [JsonProperty("gameId")]
        public int GameId { get; set; }

        [JsonProperty("builderId")]
        public int BuilderId { get; set; }

        [JsonProperty("builderName")]
        public string BuilderName { get; set; }

        [JsonProperty("bdxPartnerId")]
        public string BDXPartnerId { get; set; }

        [JsonProperty("builderImage")]
        public string BuilderImage { get; set; }

        [JsonProperty("gameName")]
        public string GameName { get; set; }

        [JsonProperty("gameVersion")]
        public int? GameVersion { get; set; }

        [JsonProperty("bdxCommunityId")]
        public string BDXCommunityId { get; set; }

        [JsonProperty("gameImages")]
        public List<GamePlanImage> GamePlanImages { get; set; }

        [JsonProperty("categories")]
        public List<GameCategory> Categories { get; set; }

        [JsonProperty("gameLeads")]
        public List<GamePlanLead> GamePlanLeads { get; set; }


        [JsonObject("category")]
        public class GameCategory
        {
            [JsonProperty("categoryId")]
            public int CategoryId { get; set; }

            [JsonProperty("name")]
            public string CategoryName { get; set; }

            [JsonProperty("eventCode")]
            public string EventCode { get; set; }

            [JsonProperty("subCategories")]
            public List<GameSubCategory> SubCategories { get; set; }

            [JsonProperty("themes")]
            public List<GameTheme> Themes { get; set; }
        }

        [JsonObject("themes")]
        public class GameTheme
        {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("gameId")]

            public int GameId { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("subCategories")]
            public List<GameSubCategory> SubCategories { get; set; }
        }

        [JsonObject("subCategory")]
        public class GameSubCategory
        {
            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("subCategoryId")]

            public int SubCategoryId { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("sequence")]
            public int Sequence { get; set; }

            [JsonProperty("values")]
            public List<EventValueObject> EventObject { get; set; }
        }

        [JsonObject("eventValueObject")]
        public class EventValueObject
        {
            [JsonProperty("eventId")]
            public int EventId { get; set; }

            [JsonProperty("eventCode")]
            public string EventCode { get; set; }

            [JsonProperty("eventImageUrl")]
            public string EventImageUrl { get; set; }

            [JsonProperty("layerName")]
            public string LayerName { get; set; }
        }

        [JsonObject("gamePlanImage")]
        public class GamePlanImage
        {

            [JsonProperty("floorId")]
            public int FloorId { get; set; }

            [JsonProperty("floorName")]
            public string FloorName { get; set; }

            [JsonProperty("floorImage")]
            public string FloorImage { get; set; }
        }

        [JsonObject("gamePlanLead")]
        public class GamePlanLead
        {
            [JsonProperty("referUser")]
            public string ReferUser { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }

            [JsonProperty("firstName")]
            public string FirstName { get; set; }

            [JsonProperty("lastName")]
            public string LastName { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("city")]
            public string City { get; set; }

            [JsonProperty("state")]
            public string State { get; set; }

            [JsonProperty("zipCode")]
            public string ZipCode { get; set; }

        }

    }

    [JsonObject("prePopulateSubCategory")]
    public class PrePopulateSubCategory
    {
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("subCategoryName ")]
        public string SubCategoryName { get; set; }
    }
}
