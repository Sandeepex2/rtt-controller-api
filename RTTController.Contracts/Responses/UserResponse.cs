﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Responses
{
    [JsonObject("user")]
    public class UserResponse
    {
        [JsonProperty("userId")]
        public int UserId { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("userType")]
        public string UserType { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("devicetoken")]
        public string DeviceToken { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        public string Token { get; set; }

        [JsonProperty("userGames")]
        public IEnumerable<UserGame> UserGames { get; set; }


        [JsonObject("userGame")]
        public class UserGame
        {
            [JsonProperty("gameId")]
            public int GameId { get; set; }

            [JsonProperty("gameName")]
            public string GameName { get; set; }

            [JsonProperty("gameVersion")]
            public int? GameVerion { get; set; }

            [JsonProperty("gameImage")]
            public string GameImamge { get; set; }

        }
    }
}
