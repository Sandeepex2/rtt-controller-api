﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using RTTController.Contracts.Requests;

namespace RTTController.Contracts.Validators
{
    public class DeleteUserGameValidator : AbstractValidator<DeleteUserGameRequest>
    {
        public DeleteUserGameValidator()
        {
            RuleFor(x => x.GameID)
                .GreaterThan(0)
                .WithMessage("Please enter valid GameID");

            RuleFor(x => x.UserID)
                .GreaterThan(0)
                .WithMessage("Please enter valid UserID");
        }
    }
}
