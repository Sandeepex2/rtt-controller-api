﻿using FluentValidation;
using RTTController.Contracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Validators
{
    public class DeleteGameCatEventValidator : AbstractValidator<DeleteGameCatEventRequest>
    {
        public DeleteGameCatEventValidator()
        {
            RuleFor(x => x.GameId)
                .GreaterThan(0)
                .WithMessage("Enter valid GameId");

            RuleFor(x => x.EventId)
                .GreaterThan(0)
                .WithMessage("Enter valid EventId");
        }
    }
}
