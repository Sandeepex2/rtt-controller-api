﻿using FluentValidation;
using RTTController.Contracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Validators
{
    public class InviteGameValidator : AbstractValidator<InviteGameRequest>
    {
        public InviteGameValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email required")
                .EmailAddress()
                .WithMessage("Please enter valid email address");

            RuleFor(x => x.GameId)
                .GreaterThan(0)
                .WithMessage("Please enter valid gameId");

            RuleFor(x => x.GameName)
                .NotEmpty()
                .WithMessage("Game name required.");
        }
    }
}
