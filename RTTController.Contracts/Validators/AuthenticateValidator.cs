﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using RTTController.Contracts.Requests;

namespace RTTController.Contracts.Validators
{
    public class AuthenticateValidator : AbstractValidator<AuthenticateRequest>
    {
        public AuthenticateValidator()
        {
            RuleFor(x => x.Username)
                .NotEmpty()
                .WithMessage("Username required.");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password required")
                .When( x=>x.Password != string.Empty)
                .Length(6, 50)
                .WithMessage("Enter valid password");
        }
    }
}
