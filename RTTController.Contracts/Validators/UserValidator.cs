﻿using FluentValidation;
using RTTController.Contracts.Requests;

namespace RTTController.Contracts.Validators
{
    public class UserValidator : AbstractValidator<UserRequest>
    {
        /// <summary>
        /// Validation rule for User
        /// </summary>
        public UserValidator()
        {
            RuleFor(x => x.UserName)
                .NotNull()
                .WithMessage("Username required");                

            RuleFor(x => x.FirstName)
                .NotNull()
                .WithMessage("Firstname required");

            RuleFor(x => x.LastName)
                .NotNull()
                .WithMessage("Lastname required");

            RuleFor(x => x.Email)
                .NotNull()
                .WithMessage("Email required")
                .EmailAddress()
                 .WithMessage("Enter valid Email");

            RuleFor(x => x.Password)
                .NotNull()
                .WithMessage("Password required")
                 .Length(6, 20)
                 .WithMessage("Password enter must be between 6-20 length");           

            RuleFor(x => x.GameId)
                .GreaterThan(0)
                .WithMessage("Enter valid GameId");

        }
    }
}
