﻿using FluentValidation;
using RTTController.Contracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Contracts.Validators
{
    public class ResetPasswordValidator : AbstractValidator<ResetPasswordRequest>
    {
        public ResetPasswordValidator()
        {
            RuleFor(x => x.Email)
                .NotNull()
                .WithMessage("Email required")
                .EmailAddress()
                .WithMessage("Enter valid email address");

            RuleFor(x => x.NewPassword)
               .Length(5, 20)
               .WithMessage("New Password enter must be between 5-20 length");

            RuleFor(x => x.ConfirmPassword)
               .Length(5, 20)
               .WithMessage("Confirm Password enter must be between 5-20 length")
               .Equal(x => x.NewPassword)
               .WithMessage("Passwords do not match");

        }
    }
}
