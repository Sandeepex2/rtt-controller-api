﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace RTTController.Repository
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        T Find(object id);
        IEnumerable<T> FromSql(string query);       
        T Add(T entity);
        T Update(T entity);
        T Delete(object id);

        bool Exists(object id);
        
    }
}
