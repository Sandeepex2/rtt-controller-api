﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace RTTController.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        private DbSet<T> _entities;

        public Repository(DbContext context)
        {
            _context = context;
        }

        private DbSet<T> Entities
        {
            get { return _entities ?? (_entities = _context.Set<T>()); }
        }

        public T Add(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            entity = Entities.Add(entity).Entity;

            return entity;
        }

        public T Delete(object id)
        {
            T entity = Find(id);

            if (entity == null)
                throw new ArgumentNullException("entity");

            entity = Entities.Remove(entity).Entity;

            return entity;
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return Entities.Where(predicate);
        }

        public T Find(object id)
        {
            return Entities.Find(id);
        }
      
        public IEnumerable<T> FromSql(string query)
        {
            return Entities.FromSql(query);
        }

        public T Update(T entity)
        {
            _context.Entry<T>(entity).State = EntityState.Modified;

            return entity;
        }

        public bool Exists(object key)
        {
            return Entities.Find(key) != null;
        }
    }
}
