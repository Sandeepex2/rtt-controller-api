﻿using RTTController.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Repository.UnitOfWork
{
    public abstract class UnitOfWork : IDisposable
    {
        public UnitOfWork(GameControllerContext context)
        {

        }

        public abstract ITransaction BeginTransaction();

        public void Dispose()
        {

        }
    }
}
