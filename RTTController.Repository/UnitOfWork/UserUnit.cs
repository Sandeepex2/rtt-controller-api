﻿using RTTController.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Repository.UnitOfWork
{
    public class UserUnit : UnitOfWork
    {
        private readonly GameControllerContext _context;

        public UserUnit(GameControllerContext context) : base(context)
        {
            _context = context;
            User = new Repository<User>(_context);
            InviteUser = new Repository<InviteUser>(_context);
            UserEmailVerification = new Repository<UserEmailVerification>(_context);
            GameUserRelationship = new Repository<GameUserRelationship>(_context);
            GamePlan = new Repository<GamePlan>(_context);
            GameLead = new Repository<GameLead>(_context);
        }
        public override ITransaction BeginTransaction()
        {
            return new Transaction(_context);
        }

        public IRepository<User> User { get; set; }
        public IRepository<InviteUser> InviteUser { get; set; }
        public IRepository<UserEmailVerification> UserEmailVerification { get; set; }

        public IRepository<GameUserRelationship> GameUserRelationship { get; set; }

        public IRepository<GamePlan> GamePlan { get; set; }

        public IRepository<GameLead> GameLead { get; set; }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

    }
}
