﻿using RTTController.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Repository.UnitOfWork
{
    public class GamePlanUnit : UnitOfWork
    {
        private readonly GameControllerContext _context;

        public GamePlanUnit(GameControllerContext context) : base(context)
        {
            _context = context;

            GamePlan = new Repository<GamePlan>(_context);
            CategoryGameMap = new Repository<CategoryGameMap>(_context);
            GamePlanCategoryValue = new Repository<GamePlanCategoryValue>(_context);
            GameLead = new Repository<GameLead>(_context);
            GamePlanImage = new Repository<GamePlanImage>(_context);
            Settings = new Repository<Settings>(_context);
            SettingOptions = new Repository<SettingOptions>(_context);
            GameUser = new Repository<User>(_context);
            PrePopulateSubCategory = new Repository<PrePopulateSubCategory>(_context);
        }
        public override ITransaction BeginTransaction()
        {
            return new Transaction(_context);
        }

        public IRepository<GamePlan> GamePlan { get; set; }
        public IRepository<CategoryGameMap> CategoryGameMap { get; set; }

        public IRepository<GamePlanCategoryValue> GamePlanCategoryValue { get; set; }
        public IRepository<GameLead> GameLead { get; set; }

        public IRepository<GamePlanImage> GamePlanImage { get; set; }

        public IRepository<Settings> Settings { get; set; }

        public IRepository<SettingOptions> SettingOptions { get; set; }

        public IRepository<User> GameUser { get; set; }

        public IRepository<PrePopulateSubCategory> PrePopulateSubCategory { get; set; }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }


    }
}
