﻿using RTTController.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Repository.UnitOfWork
{
    public class SharedUnit : UnitOfWork
    {
        private readonly GameControllerContext _context;

        public SharedUnit(GameControllerContext context) : base(context)
        {
            _context = context;

            Theme = new Repository<Theme>(_context);
            Category = new Repository<Category>(_context);
            SubCategory = new Repository<SubCategory>(_context);
            Builder = new Repository<Builder>(_context);
            CategoryGameMap = new Repository<CategoryGameMap>(_context);
            CategorySubCategoryMapping = new Repository<CategorySubCategoryMapping>(_context);
            GamePlanCategoryValue = new Repository<GamePlanCategoryValue>(_context);
            Game = new Repository<GamePlan>(_context);
            GamePlanImage = new Repository<GamePlanImage>(_context);
        }
        public override ITransaction BeginTransaction()
        {
            return new Transaction(_context);
        }

        public IRepository<GamePlan> Game { get; set; }
        public IRepository<Category> Category { get; set; }
        public IRepository<SubCategory> SubCategory { get; set; }
        public IRepository<CategoryGameMap> CategoryGameMap { get; set; }
        public IRepository<Builder> Builder { get; set; }
        public IRepository<Theme> Theme { get; set; }
        public IRepository<CategorySubCategoryMapping> CategorySubCategoryMapping { get; set; }
        public IRepository<GamePlanCategoryValue> GamePlanCategoryValue { get; set; }
        public IRepository<GamePlanImage> GamePlanImage { get; set; }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

    }
}
