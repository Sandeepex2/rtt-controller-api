﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Repository
{
    public interface ITransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
