﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Core.API.Models;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuilderController : ControllerBase
    {
        #region private members       
        private IBuilderService _builderService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(BuilderController));
        private readonly AppSettings _appSettings;

        #endregion

        #region Constructor
        /// <summary>
        /// Construct BuilderController object
        /// </summary>
        /// <param name="builderService"></param>
        /// <param name="cloudService"></param>
        public BuilderController(IBuilderService builderService, IOptions<AppSettings> appSettings, ICloudService cloudService)
        {
            _builderService = builderService ?? throw new ArgumentNullException("BuilderService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
        }
        #endregion

        #region GET
        /// <summary>
        /// Retreives the Builder by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var user = _builderService.GetBuilderById(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// Retreives Builder List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var users = _builderService.GetAllBuilder();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = users });
        }
        #endregion

        #region POST

        /// <summary>
        /// Delete Builder
        /// </summary>
        /// <returns></returns>
        [HttpPost("delete/{id:int}")]
        public IActionResult Delete(int Id)
        {
            var users = _builderService.DeleteBuilder(Id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = users });
        }

        /// <summary>
        /// Create Builder
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<IActionResult> POST()
        {
            BuilderResponse response = null;
            IFormFile file;
            BuilderRequest request = new BuilderRequest();

            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".jpeg", ".png" };


            var httpRequest = HttpContext.Request;

            #region Validation            

            if (!httpRequest.Form.ContainsKey("name"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Builder name is missing"));

            if (httpRequest.Form.Files.Count == 0)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload Builder Image"));

            if (_cloudService.IsBuilderExists(httpRequest.Form["name"]))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Builder {httpRequest.Form["name"]} already exist."));

            #endregion

            request.Name = httpRequest.Form["name"];

            if( String.IsNullOrEmpty(request.Name))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Builder name required."));


            if (httpRequest.Form.Files.Count > 0)
            {
                file = httpRequest.Form.Files[0];

                //Is the file too big to upload?
                decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                if (size > (4 * 1024))
                {
                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                }

                var extension = file.FileName.GetFileExtension();

                if (!AllowedFileExtensions.Contains(extension.ToLower()))
                {

                    var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png");

                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, message));
                }
                var imgresponse = await _cloudService.UploadFile(file);

                if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                {
                    request.LogoUrl = imgresponse.FileName;
                }
                else
                    throw new Exception(imgresponse.FileName);                

                _logger.Info("Create Builder request" + JsonConvert.SerializeObject(request));

                 response = _builderService.CreateBuilder(request);

                
            }
            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }

        /// <summary>
        /// Update Builder
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public async Task<IActionResult> UpdateBuilder()
        {
            BuilderResponse response = null;
            IFormFile file;
            BuilderRequest request = new BuilderRequest();

            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".jpeg", ".png" };
            var httpRequest = HttpContext.Request;

            #region Validation

            if (!httpRequest.Form.ContainsKey("id"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Builder id is missing"));

            if (!httpRequest.Form.ContainsKey("name"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Builder name is missing"));

            if (!_cloudService.IsBuilderExists(Convert.ToInt32(httpRequest.Form["id"])))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Builder {httpRequest.Form["id"]} does not exist."));

            if (_cloudService.IsBuilderExists(Convert.ToInt32(httpRequest.Form["id"]),httpRequest.Form["name"]))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Builder {httpRequest.Form["name"]} already exist."));

            #endregion

            request.Id = Convert.ToInt32(httpRequest.Form["id"]);
            request.Name = httpRequest.Form["name"];

            if (String.IsNullOrEmpty(request.Name))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Builder name required."));

            if (httpRequest.Form.Files.Count > 0)
            {
                file = httpRequest.Form.Files[0];

                //Is the file too big to upload?
                decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                if (size > (4 * 1024))
                {
                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                }

                var extension = file.FileName.GetFileExtension();

                if (!AllowedFileExtensions.Contains(extension.ToLower()))
                {

                    var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png");

                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, message));
                }
                var imgresponse = await _cloudService.UploadFile(file);

                if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                {
                    request.LogoUrl = imgresponse.FileName;
                }
                else
                    throw new Exception(imgresponse.FileName);
            }
            else
                request.LogoUrl = "";

            _logger.Info("Update Builder request" + JsonConvert.SerializeObject(request));
            response = _builderService.UpdateBuilder(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }


        #endregion
    }
}