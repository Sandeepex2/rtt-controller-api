﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RTTController.Contracts.Requests;
using RTTController.Core.API.Models;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{    
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region private members       
        private IUserService _userService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(UserController));
        private readonly AppSettings _appSettings;

        #endregion

        #region Constructor
        /// <summary>
        /// Construct UserController object
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="appSettings"></param>
        public UserController(IUserService userService, IOptions<AppSettings> appSettings , ICloudService cloudService)
        {
            _userService = userService ?? throw new ArgumentNullException("UserService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
        }
        #endregion

        #region GET
        /// <summary>
        /// Retreives the User by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var user = _userService.GetUserById(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// Retreives User List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var users = _userService.GetAllUsers();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = users });
        }

        /// <summary>
        ///  Retreives the User by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet("email:email")]
        public IActionResult Get( string email)
        {
            _logger.Info("Get user data by email id:" + email);
            var user = _userService.GetUserByEmail(email);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        ///  Retreives the Invite User List
        /// </summary>        
        /// <returns></returns>
        [HttpGet("invitelist")]
        public IActionResult GetInviteList()
        {
            var inviteUsers = _userService.GetInviteUserList();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = inviteUsers });
        }

        /// <summary>
        /// Verify Inviter User Email token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("verifytoken/{token:guid}")]
        [AllowAnonymous]
        public IActionResult VerifyEmailToken(Guid token)
        {
            var result = _userService.VerifyEmailToken(token.ToString());

            if (result != null)
                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = result });
            else
                return NotFound("Invite User detail for token " + token.ToString() + " not found");
        }
        #endregion

        #region POST

        /// <summary>
        /// Update device token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("updatetoken")]
        public IActionResult UpdateToken([FromBody] DeviceTokenRequest request)
        {
            if (request == null)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Authenticate request cannot be passed null."));

            _logger.Info("Update Device Token Request:" + JsonConvert.SerializeObject(request));

            var response = _userService.UpdateDeviceToken(request);
            
            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }


        /// <summary>
        /// Authenticate User
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateRequest request)
        {
            if (request == null)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Authenticate request cannot be passed null."));

            _logger.Info("Authenticate User Request:" + JsonConvert.SerializeObject(request));

            var response = _userService.Authenticate(request);

            if (response == null)
                return Unauthorized(new ApiResponse(StatusCodes.Status401Unauthorized, "The user name or password is incorrect."));

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }

        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> Create([FromBody] UserRequest request)
        {
            if (request == null)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Authenticate request cannot be passed null."));

            _logger.Info("Create User Request:" + JsonConvert.SerializeObject(request));

            if (!_cloudService.IsGameExists(request.GameId))
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound, "Game does not exist"));

            if (_cloudService.IsUserExists(request.Email))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Email {request.Email} already exist."));

            var response =await _userService.CreateUser(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }

        /// <summary>
        ///  Invite user for particular game
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("invite")]
        public async Task<IActionResult> InviteUser([FromBody] InviteGameRequest request)
        {
            if (request == null)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "InviteUser request cannot be passed null."));

            _logger.Info("Invite User Request:" + JsonConvert.SerializeObject(request));

            if (!_cloudService.IsGameExists(request.GameId))                
              return NotFound(new ApiResponse(StatusCodes.Status404NotFound, "Game does not exist"));

            if(_userService.IsGameAlreadyAssignedToUser(request.Email , request.GameId))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"User {request.Email} already invited for the game {request.GameName}."));

            var response = await _userService.InviteUser(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }

        /// <summary>
        ///  User unassigned the game
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("unassignedgame")]
        public IActionResult DeleteUserGame(DeleteUserGameRequest request)
        {
            if (request == null)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "DeleteUserGame request cannot be passed null."));

            _logger.Info("Delete User Game Request:" + JsonConvert.SerializeObject(request));

            if (!_cloudService.IsGameExists(request.GameID))
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound, "Game does not exist"));

            if (!_cloudService.IsUserExists(request.UserID))
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound, "User does not exist"));

            var response = _userService.DeleteUserGame(request);

            if (response)
            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
            else
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, "No record found") { Data = response});

        }
        #endregion
    }
}