﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Core.API.Models;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        #region private members       
        private ICategoryService _catService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(CategoryController));

        #endregion

        #region Constructor
        /// <summary>
        /// Construct CategoryController object
        /// </summary>
        /// <param name="catService"></param>
        /// <param name="cloudService"></param>
        public CategoryController(ICategoryService catService, ICloudService cloudService)
        {
            _catService = catService ?? throw new ArgumentNullException("catService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
        }
        #endregion

        #region GET
        /// <summary>
        /// Retreives the Category by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var user = _catService.GetCategory(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// Retreives Category List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var users = _catService.Get();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = users });
        }
        #endregion

        #region POST
        /// <summary>
        /// Create Category
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult POST([FromBody] CategoryRequest request)
        {
            CategoryResponse response = null;

            if (request == null) throw new ArgumentNullException("Category request cannot be passed null");

            if (string.IsNullOrEmpty(request.Name))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Category Name cannot be null or empty."));

            if (string.IsNullOrEmpty(request.EventCode))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Category EventCode cannot be null or empty."));

            if (_cloudService.IsCategoryExists(request.Name))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Category {request.Name} already exist."));


            response = _catService.Create(request);

            return Ok(response);
        }
        #endregion
    }
}