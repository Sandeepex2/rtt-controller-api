﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RTTController.Contracts.Requests;
using RTTController.Core.API.Models;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/password")]
    [ApiController]
    public class ForgotPasswordController : ControllerBase
    {
        #region private members
        private IEmailVerificationService _emailService;
        private ILog _logger = LogManager.GetLogger(typeof(ForgotPasswordController));
        #endregion

        #region Constructor
        /// <summary>
        /// Construct Email Verification service object
        /// </summary>
        /// <param name="emailService"></param>
        public ForgotPasswordController(IEmailVerificationService emailService)
        {
            _emailService = emailService ?? throw new ArgumentNullException("EmailVerificationService reference cannot be passed null");
        }
        #endregion

        #region GET
        /// <summary>
        /// Request for reset password. User will receive token to their email id.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get(string email)
        {
            if (!_emailService.IsExists(email))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, "Email does not exists !"));

            return Ok(new ApiResponse(StatusCodes.Status200OK, "Success") { Data = await _emailService.Request(email) });
        }

        /// <summary>
        /// Verify Reset email Token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("resettoken")]
        [AllowAnonymous]
        public IActionResult GetResetToken(string token)
        {
            var response = _emailService.VerifyResetToken(token);

            if(response != null)            
               return Ok(new ApiResponse(StatusCodes.Status200OK, "Success") { Data = _emailService.VerifyResetToken(token) });
            else
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, "Invalid Token"));
        }
        #endregion

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("reset")]
        [AllowAnonymous]
        public IActionResult Reset([FromBody] ResetPasswordRequest request)
        {
            return Ok(new ApiResponse(StatusCodes.Status200OK, "Success") { Data = _emailService.Reset(request) });
        }

        /// <summary>
        /// User Change Password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("change")]
        public IActionResult Change([FromBody] ChangePasswordRequest request)
        {
            if (request == null)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "ChangePassword request cannot be passed null."));

            string response = Convert.ToString(_emailService.Change(request));

            if (response.ToLower() == "true")
                return Ok(new ApiResponse(StatusCodes.Status200OK, "Success") { Data = response });
            else
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, response));
        }
    }
}