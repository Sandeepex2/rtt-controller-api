﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RTTController.Contracts.Requests;
using RTTController.Core.API.Models;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThemeController : ControllerBase
    {
        #region private members       
        private IThemeService _themeService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(ThemeController));
        private readonly AppSettings _appSettings;

        #endregion

        #region Constructor
        /// <summary>
        /// Construct BuilderController object
        /// </summary>
        /// <param name="themeService"></param>
        /// <param name="appSettings"></param>
        /// <param name="cloudService"></param>
        public ThemeController(IThemeService themeService, IOptions<AppSettings> appSettings, ICloudService cloudService)
        {
            _themeService = themeService ?? throw new ArgumentNullException("ThemeService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
        }
        #endregion

        #region GET
        /// <summary>
        /// Retreives the Theme by GameId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var data = _themeService.GetByGameId(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }

        /// <summary>
        /// Retreives Theme List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var data = _themeService.Get();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }
        #endregion

        #region POST

        /// <summary>
        /// Delete Builder
        /// </summary>
        /// <returns></returns>
        [HttpPost("delete/{id:int}")]
        public IActionResult Delete(int Id)
        {
            var data = _themeService.Delete(Id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }

        /// <summary>
        /// Create Theme
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult POST(ThemeRequest request)
        {
            var data = _themeService.Create(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }

        /// <summary>
        /// Update Builder
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult UpdateBuilder(ThemeRequest request)
        {
            var data = _themeService.Update(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }


        #endregion

    }
}