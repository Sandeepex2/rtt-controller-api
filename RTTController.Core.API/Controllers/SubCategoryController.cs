﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Core.API.Models;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubCategoryController : ControllerBase
    {
        #region private members       
        private ISubCategoryService _subCatService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(SubCategoryController));

        #endregion

        #region Constructor
        /// <summary>
        /// Construct SubCategoryController object
        /// </summary>
        /// <param name="subCatService"></param>
        /// <param name="cloudService"></param>
        public SubCategoryController(ISubCategoryService subCatService, ICloudService cloudService)
        {
            _subCatService = subCatService ?? throw new ArgumentNullException("subCatService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
        }
        #endregion

        #region GET
        /// <summary>
        /// Retreives the Subcategory by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var user = _subCatService.GetById(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// Retreives Subcategory List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var users = _subCatService.Get();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = users });
        }
        #endregion

        #region POST
        /// <summary>
        /// Create Subcategory
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult POST([FromBody] SubCategoryRequest request)
        {
            SubCategoryResponse response = null;

            if (request == null) throw new ArgumentNullException("SubCategory request cannot be passed null");

            if (string.IsNullOrEmpty(request.Name))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "SubCategory Name cannot be null or empty."));

            if (request.CategoryId == 0)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "CategoryId cannot be passed zero."));

            if (_cloudService.IsSubCategoryExistsInGame(request.Name, request.CategoryId, request.GameId,request.ThemeId))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"SubCategory {request.Name} already exist."));

            response = _subCatService.Create(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }


        /// <summary>
        /// Update Subcategory master
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("update")]
        public IActionResult Update([FromBody] SubCategoryUpdateRequest request)
        {
            SubCategoryResponse response = null;

            if (request == null) throw new ArgumentNullException("SubCategory request cannot be passed null");

            if (_cloudService.IsSubCategoryExists(request.Id, request.Name))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"SubCategory {request.Name} already exist."));

            if (!_cloudService.IsSubCategoryExists(request.Id))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"SubCategory with Id: {request.Id} does not exist."));

            response = _subCatService.Update(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }
        #endregion

    }
}