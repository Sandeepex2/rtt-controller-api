﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Core.API.Models;
using RTTController.Entities.Enums;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/game")]
    [ApiController]
    public class GamePlanController : ControllerBase
    {
        #region private members
        private IGameService _gameService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(GamePlanController));
        private readonly AppSettings _appSettings;

        #endregion

        #region Constructor
        /// <summary>
        /// Construct GamePlan Controller Object
        /// </summary>        
        /// <param name="gameService"></param>
        /// <param name="appSettings"></param>
        /// <param name="cloudService"></param>
        public GamePlanController(IGameService gameService, IOptions<AppSettings> appSettings, ICloudService cloudService)
        {            
            _gameService = gameService ?? throw new ArgumentNullException("GameService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
        }
        #endregion

        #region GET
        /// <summary>
        /// Retreive game by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var user = _gameService.Get(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// Retreives all game plans
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var users = _gameService.Get();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = users });
        }

        /// <summary>
        /// LoadSettings
        /// </summary>        
        /// <returns></returns>
        [HttpGet("settings")]
        public IActionResult LoadSettings()
        {
            var user = _gameService.LoadSettings();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("delete/{id:int}")]
        public IActionResult DeleteGame(int id)
        {
            var status = _gameService.DeleteGame(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = status });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("subcategory/delete/{id:int}")]
        public IActionResult DeleteSubcategory(int id)
        {
            var gameId = _cloudService.GetGameId(id, Contracts.RequestType.SubCategory);

            var status = _gameService.DeleteSubcategory(id);

            if (true)
                _cloudService.UpdateGameVersion(gameId); //Update game version on successful subcategory deletion from game.

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = status });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("subcategory/update/sequence")]
        public IActionResult SequenceUpdate(SubCategorySequecneUpdateRequest request)
        {
            var status = _gameService.SequenceUpdate(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = status });
        }



        #endregion

        #region POST


        /// <summary>
        /// Create Game
        /// </summary>
        /// <returns></returns>
        [HttpPost("plan/update")]
        public IActionResult UpdatePlan(PlanRequest request)
        {
            var plan = _gameService.Get(request.Id);

            if (_cloudService.IsGameExists(request.Name, plan.BuilderId))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, request.Name + " Already exists!"));

            var status = _gameService.UpdatePlan(request);
            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = status });
        }

        /// <summary>
        /// Create Game
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<IActionResult> POST()
        {
            IFormFile file;
            GamePlanRequest gamePlanRequest = new GamePlanRequest();
            List<GameFloorImage> listGameImages = new List<GameFloorImage>();

            IList<string> AllowedFileExtensions = new List<string> { ".svg" };


            var httpRequest = HttpContext.Request;

            #region Validation

            if (!httpRequest.Form.ContainsKey("builderId"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "BuilderId is missing"));

            if (!httpRequest.Form.ContainsKey("name"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Game name is missing"));

            if (httpRequest.Form.Files.Count == 0)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload Floor Image"));

            if (_cloudService.IsGameExists(httpRequest.Form["name"], Convert.ToInt32(httpRequest.Form.ContainsKey("builderId"))))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Game {httpRequest.Form["name"]} already exist."));

            #endregion

            gamePlanRequest.BuilderId = Convert.ToInt32(httpRequest.Form["builderId"]);
            gamePlanRequest.GameName = httpRequest.Form["name"];

            if (httpRequest.Form.Files.Count > 0)
            {
                foreach (var fileData in httpRequest.Form.Files)
                {
                    file = fileData;

                    //Is the file too big to upload?
                    decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                    if (size > (4 * 1024))
                    {
                        return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                    }

                    var extension = file.FileName.GetFileExtension();

                    if (!AllowedFileExtensions.Contains(extension.ToLower()))
                    {

                        //var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png,.svg.");

                        return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload image of type .svg"));
                    }

                    var imgresponse = await _cloudService.UploadFile(file);

                    if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                    {
                        listGameImages.Add(new GameFloorImage
                        {
                            FloorId = (int)EnumHelper.GetEnumValue<FloorType>(file.Name),
                            FloorImage = imgresponse.FileName
                        });
                    }
                    else
                        throw new Exception(imgresponse.FileName);
                }
                gamePlanRequest.GamePlanImages = listGameImages;

                _logger.Info("Create game plan request" + JsonConvert.SerializeObject(gamePlanRequest));

                var response = _gameService.CreateGame(gamePlanRequest);

                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
            }

            return BadRequest(new ApiResponse(StatusCodes.Status404NotFound, "No file found to upload"));
        }


        /// <summary>
        /// Update Values
        /// </summary>
        /// <returns></returns>
        [HttpPost("values/update")]
        public IActionResult UpdateValues(ValueRequest request)
        {
            var response = _gameService.UpdateValues(request);
            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
        }


        /// <summary>
        /// Update the game and add the GamePlan  Event data
        /// </summary>
        /// <returns></returns>
        [HttpPost("update")]
        public async Task<IActionResult> UpdateGame()
        {
            IFormFile file;
            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".png", ".svg" };
            BaseApiResponse response = null;

            var httpRequest = HttpContext.Request;

            _logger.Info(httpRequest.Form);

            #region Validation

            if (!httpRequest.Form.ContainsKey("gameId"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "gameId is missing"));

            if (!httpRequest.Form.ContainsKey("categoryId"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "CategoryId is missing"));

            if (!httpRequest.Form.ContainsKey("subCategoryId"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "SubCategoryId is missing"));

            if (httpRequest.Form.Files.Count == 0)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload Image"));

            #endregion

            if (httpRequest.Form.Files.Count > 0)
            {
                GamePlanRequest gamePlanRequest = new GamePlanRequest();
                List<GamePlanCategoryEvent> listGameEvents = new List<GamePlanCategoryEvent>();

                foreach (var fileData in httpRequest.Form.Files)
                {
                    file = fileData;

                    //Is the file too big to upload?
                    decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                    if (size > (4 * 1024))
                    {
                        return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                    }

                    var extension = file.FileName.GetFileExtension();

                    if (!AllowedFileExtensions.Contains(extension.ToLower()))
                    {

                        var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png,.svg.");

                        return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, message));
                    }

                    var imgresponse = await _cloudService.UploadFile(file);

                    if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                    {
                        var eventCode = file.Name.Substring(file.Name.IndexOf("_") + 1);

                        if (_cloudService.IsEventCodeExistsInSubCategory(Convert.ToInt32(httpRequest.Form["gameId"]), Convert.ToInt32(httpRequest.Form["categoryId"]), Convert.ToInt32(httpRequest.Form["subCategoryId"]), eventCode, Convert.ToInt32(httpRequest.Form["themeId"])))
                            return Conflict(new ApiResponse(StatusCodes.Status409Conflict, eventCode + " Already exists!"));

                        if(listGameEvents.Any(x=>x.EventCode == eventCode))
                          return Conflict(new ApiResponse(StatusCodes.Status409Conflict, "Enter unique event code."));

                        listGameEvents.Add(new GamePlanCategoryEvent
                        {
                            EventCode = eventCode,
                            ImageUrl = imgresponse.FileName,
                            LayerName = file.Name.Contains('#') ? file.Name.Substring(0, file.Name.IndexOf("#")) : ""
                        });
                    }
                }

                CategoryGameMapping categoryGameMap = new CategoryGameMapping
                {
                    GameId = Convert.ToInt32(httpRequest.Form["gameId"]),
                    CategoryId = Convert.ToInt32(httpRequest.Form["categoryId"]),
                    ThemeId = Convert.ToInt32(httpRequest.Form["themeId"]),
                    SubCategoryId = Convert.ToInt32(httpRequest.Form["subCategoryId"]),
                    GamePlanCategoryEvents = listGameEvents
                };

                gamePlanRequest.GameId = Convert.ToInt32(httpRequest.Form["gameId"]);
                gamePlanRequest.CategoryGameMap = categoryGameMap;

                _logger.Info("Update Game request:" + JsonConvert.SerializeObject(gamePlanRequest));

                response = _gameService.UpdateGame(gamePlanRequest);
            }

            if (response.StatusCode == StatusCodes.Status200OK)
                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response.Result });
            else
                return NotFound(new ApiResponse(StatusCodes.Status404NotFound, response.Message));
        }




        /// <summary>
        /// Update Game Plan Floor Image
        /// </summary>
        /// <returns></returns>
        [HttpPost("floorplan/update")]
        public async Task<IActionResult> UpdateGamePlanImage()
        {
            IFormFile file;
            UpdateGameFloorImageRequest request = new UpdateGameFloorImageRequest();


            IList<string> AllowedFileExtensions = new List<string> { ".svg" };


            var httpRequest = HttpContext.Request;

            #region Validation

            if (!httpRequest.Form.ContainsKey("gameId"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "GameId is missing"));

            if (!httpRequest.Form.ContainsKey("gameFloorID"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "FloorId is missing"));

            if (httpRequest.Form.Files.Count == 0)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload Floor Image"));

            if (!_cloudService.IsGameExists(Convert.ToInt32(httpRequest.Form["gameId"])))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Game does not exist."));

            #endregion

            request.GameID = Convert.ToInt32(httpRequest.Form["gameId"]);
            request.GameFloorID = Convert.ToInt32(httpRequest.Form["gameFloorID"]);


            if (httpRequest.Form.Files.Count > 0)
            {
                file = httpRequest.Form.Files[0];
                //Is the file too big to upload?
                decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                if (size > (4 * 1024))
                {
                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                }

                var extension = file.FileName.GetFileExtension();

                if (!AllowedFileExtensions.Contains(extension.ToLower()))
                {

                    //var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png,.svg.");

                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload image of type .svg"));
                }

                var imgresponse = await _cloudService.UploadFile(file);

                if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                {
                    request.ImageName = imgresponse.FileName;
                }
                else
                    throw new Exception(imgresponse.FileName);

                _logger.Info("Update Game Plan Floor Image" + JsonConvert.SerializeObject(request));

                var response = _gameService.UpdateGameFloorImage(request);

                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });

            }

            return BadRequest(new ApiResponse(StatusCodes.Status404NotFound, "No file found to upload"));
        }


        /// <summary>
        /// Update Game Plan Floor Image
        /// </summary>
        /// <returns></returns>
        [HttpPost("floorplan/add")]
        public async Task<IActionResult> AddGamePlanImage()
        {
            AddGameFloorImageRequest request = new AddGameFloorImageRequest();
            List<GameFloorImage> listGameImages = new List<GameFloorImage>();

            IList<string> AllowedFileExtensions = new List<string> { ".svg" };
            var httpRequest = HttpContext.Request;

            #region Validation

            if (!httpRequest.Form.ContainsKey("gameId"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "GameId is missing"));

            if (httpRequest.Form.Files.Count == 0)
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload Floor Image"));

            if (!_cloudService.IsGameExists(Convert.ToInt32(httpRequest.Form["gameId"])))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"Game does not exist."));

            #endregion

            request.GameID = Convert.ToInt32(httpRequest.Form["gameId"]);

            if (httpRequest.Form.Files.Count > 0)
            {
                foreach (var file in httpRequest.Form.Files)
                {
                    //Is the file too big to upload?
                    decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                    if (size > (4 * 1024))
                    {
                        return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                    }

                    var extension = file.FileName.GetFileExtension();

                    if (!AllowedFileExtensions.Contains(extension.ToLower()))
                    {
                        //var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png,.svg.");

                        return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Please upload image of type .svg"));
                    }

                    var imgresponse = await _cloudService.UploadFile(file);

                    if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                    {
                        listGameImages.Add(new GameFloorImage
                        {
                            FloorId = (int)EnumHelper.GetEnumValue<FloorType>(file.Name),
                            FloorImage = imgresponse.FileName
                        });
                    }
                    else
                        throw new Exception(imgresponse.FileName);
                }

                request.GamePlanImages = listGameImages;
                _logger.Info("Add Game Plan Floor Image" + JsonConvert.SerializeObject(request));
                var response = _gameService.AddGameFloorImage(request);

                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = response });
            }

            return BadRequest(new ApiResponse(StatusCodes.Status404NotFound, "No file found to upload"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("floorplan/delete/{id:int}")]
        public IActionResult DeleteGameFloor(int id)
        {
            var gameId = _cloudService.GetGameId(id, Contracts.RequestType.PlanSvg);

            var status = _gameService.DeleteGameFloor(id);

            if (true)
                _cloudService.UpdateGameVersion(gameId); //Update game version on successful game floor deletion.

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = status });
        }



        /// <summary>
        /// Delete the Category game event by id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("deleteevent")]
        public async Task<IActionResult> Delete([FromBody] DeleteGameCatEventRequest request)
        {
            if (request == null) throw new ArgumentNullException("DeleteGameEvent request cannot be passed null");

            if (!_cloudService.IsGameExists(request.GameId))
                return Conflict(new ApiResponse(StatusCodes.Status409Conflict, $"No record  found for game id {request.GameId}"));
           
            var data = await _gameService.DeleteGameCategoryEvent(request.GameId, request.EventId);

            if (true)
                _cloudService.UpdateGameVersion(request.GameId); //Update game version on successful event deletion.

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("settings/add")]
        public IActionResult AddSettings(SettingsRequest request)
        {
            var user = _gameService.AddSettings(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("settings/update")]
        public IActionResult UpdateSettings(SettingsRequest request)
        {
            var user = _gameService.UpdateSettings(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = user });
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("prepopulatesubcategory/add")]
        public IActionResult AddPrePopulateSubCategory(PrePopulateSubCategory request)
        {
            var data = _gameService.CreatePrePopulateSubCategory(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("prepopulatesubcategory/update")]
        public IActionResult UpdatePrePopulateSubCategory(PrePopulateSubCategory request)
        {
            var data = _gameService.UpdatePrePopulateSubCategory(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("prepopulatesubcategory/Get")]
        public IActionResult GetPrePopulateSubCategory(int id)
        {
            var data = _gameService.GetPrePopulateSubCategory(id);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("prepopulatesubcategory/list")]
        public IActionResult ListPrePopulateSubCategory()
        {
            var data = _gameService.ListPrePopulateSubCategory();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
        }



        #endregion


    }
}