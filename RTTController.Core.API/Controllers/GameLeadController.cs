﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Core.API.Models;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;

namespace RTTController.Core.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameLeadController : ControllerBase
    {
        #region private members       
        private IGameLeadService _gameLeadService;
        private ICloudService _cloudService;
        private ILog _logger = LogManager.GetLogger(typeof(GameLeadController));
        private readonly AppSettings _appSettings;

        #endregion

        #region Constructor
        /// <summary>
        /// Construct GamePlan Controller Object
        /// </summary>
        /// <param name="gameLeadService"></param>
        /// <param name="appSettings"></param>
        /// <param name="cloudService"></param>
        public GameLeadController(IGameLeadService gameLeadService, IOptions<AppSettings> appSettings, ICloudService cloudService)
        {
            _gameLeadService = gameLeadService ?? throw new ArgumentNullException("GameLeadService reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
        }
        #endregion

        #region GET
        /// <summary>
        /// Retrieves all the game leads
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetGameLeads()
        {
            var result = _gameLeadService.GetGameLeads();

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = result });
        }

        /// <summary>
        /// Get Game Leads
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("leads")]
        public IActionResult GetGameLeads(LeadRequest request)
        {
            GameLeadResponse gameLeads = null;

            int gameId = request.GameId ?? 0;
            int userId = request.UserId ?? 0;

            if (gameId != 0 && userId != 0)
            {
                gameLeads = _gameLeadService.GetGameLead(gameId, userId);
            }
            else if (gameId != 0)
            {
                gameLeads = _gameLeadService.GetGameLeadByGameId(gameId);
            }
            else if (userId != 0)
            {
                gameLeads = _gameLeadService.GetGameLeadByUserId(userId);
            }
            else
            {
                gameLeads = _gameLeadService.GetGameLeads();
            }

            if (gameLeads != null)
                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = gameLeads });
            else
                return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = new GameUserLead[] { } });
        }

        /// <summary>
        /// Reteive the game leads by gameid
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        [HttpGet("{gameId:int}")]
        public IActionResult GetGameLeads(int gameId)
        {
            var gameLeads = _gameLeadService.GetGameLeadByGameId(gameId);

            return Ok(gameLeads);
        }

        /// <summary>
        ///  Retrieves the Game Leads by passing gamid and userId
        /// </summary>
        /// <param name="gameid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet("{gameid=gameid}/{userid=userid}")]
        public IActionResult GetGameLead(int gameid, int userid)
        {
            var gameLeads = _gameLeadService.GetGameLead(gameid, userid);

            return Ok(gameLeads);
        }

        #endregion

        #region POST
        /// <summary>
        /// Create Game Lead
        /// </summary>
        /// <param name="request"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> POST([ModelBinder(BinderType = typeof(JsonModelBinder))] GameLeadRequest request , IFormFile file)
        {
            IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".jpeg", ".png" };

            var httpRequest = HttpContext.Request;

            if (!httpRequest.Form.ContainsKey("request"))
                return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "GameLead Request is missing"));

            if (file!= null && file.Length > 0)
            {
                //Is the file too big to upload?
                decimal size = Math.Round(((decimal)file.Length / (decimal)1024), 2);
                if (size > (4 * 1024))
                {
                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, "Filesize of image is too large. Maximum file size permitted is 4MB"));
                }

                var extension = file.FileName.GetFileExtension();

                if (!AllowedFileExtensions.Contains(extension.ToLower()))
                {

                    var message = string.Format("Please Upload image of type .jpg,.jpeg,.gif,.png,.svg.");

                    return BadRequest(new ApiResponse(StatusCodes.Status400BadRequest, message));
                }
                var imgresponse = await _cloudService.UploadFile(file);

                if (imgresponse.Status == System.Net.HttpStatusCode.OK)
                {
                    request.Photo = imgresponse.FileName;
                }
            }
            var data = _gameLeadService.CreateGameLead(request);

            return Ok(new ApiResponse(StatusCodes.Status200OK) { Data = data });
            
            //else
            //{
            //    return BadRequest(new ApiResponse(StatusCodes.Status404NotFound, "No file found to upload"));
            //}
                
        }
        #endregion
    }
}