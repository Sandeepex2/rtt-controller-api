﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Validators;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Services;
using RTTController.Services.Services.Interfaces;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace RTTController.Core.API.Extensions
{
    public static class ServiceExtensions
    {
        /// <summary>
        ///  Configire CORS Policy
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
        }

        /// <summary>
        ///  Configure Service DI
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureServiceDependencies(this IServiceCollection services)
        {
            services.AddScoped<UserUnit>();
            services.AddScoped<SharedUnit>();
            services.AddScoped<GamePlanUnit>();            
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICloudService, CloudService>();
            services.AddScoped<IGameService, GameService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ISubCategoryService, SubCategoryService>();
            services.AddScoped<IBuilderService, BuilderService>();
            services.AddScoped<IEmailVerificationService, EmailVerificationService>();
            services.AddScoped<IGameLeadService, GameLeadService>();
            services.AddScoped<IThemeService, ThemeService>();
        }

        /// <summary>
        /// Configure Request Validators
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureRequestValidators(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<AuthenticateRequest>, AuthenticateValidator>();
            services.AddSingleton<IValidator<UserRequest>, UserValidator>();
            services.AddSingleton<IValidator<InviteGameRequest>, InviteGameValidator>();
            services.AddSingleton<IValidator<ResetPasswordRequest>, ResetPasswordValidator>();
            services.AddSingleton<IValidator<ChangePasswordRequest>, ChangePasswordValidator>();
            services.AddSingleton<IValidator<DeleteGameCatEventRequest>, DeleteGameCatEventValidator>();
            services.AddSingleton<IValidator<DeleteUserGameRequest>, DeleteUserGameValidator>();
        }

        /// <summary>
        /// Configure Swagger
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "RTT Controller API",
                    Description = "API for RTT Controller Apps",
                    TermsOfService = "None"
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                   { "Bearer", Enumerable.Empty<string>() },
               });
            });
        }
    }
}
