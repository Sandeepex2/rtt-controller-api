﻿using log4net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;


namespace RTTController.Core.API.Middleware
{
    public static class ExceptionMiddlewareExtensions
    {

        /// <summary>
        /// For exception log
        /// </summary>
        /// <param name="app"></param>
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    ILog _logger = LogManager.GetLogger(typeof(ExceptionMiddlewareExtensions));
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    string queryString = context.Request.QueryString.HasValue ? context.Request.QueryString.Value : string.Empty;
                    string requestBody = string.Empty;
                    //context.Request.Body.Seek(0, SeekOrigin.Begin);
                    using (var streamReader = new StreamReader(context.Request.Body, Encoding.UTF8))
                    {
                        requestBody = streamReader.ReadToEnd();
                    }
                    requestBody = string.IsNullOrEmpty(requestBody) ? string.Empty : "Body = " + requestBody;
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

                    var error = contextFeature != null ? contextFeature.Error : new Exception();
                    _logger.Error($"Something went wrong: URL {context.Request.Path}{queryString}  {requestBody} \n {error}");

                    if (contextFeature != null)
                    {
                        var result = JsonConvert.SerializeObject(new { statusCode = context.Response.StatusCode, message = "Internal Server Error." });
                        //await context.Response.WriteAsync(new
                        //{
                        //    StatusCode = context.Response.StatusCode,
                        //    Message = "Internal Server Error."
                        //}.ToString());

                        await context.Response.WriteAsync(result);
                    }
                });
            });
        }

    }
}
