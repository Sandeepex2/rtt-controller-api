﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace RTTController.Entities.Enums
{
    public enum UserStatus
    {
        Active = 1,
        Locked = 2,
        PasswordChangeRequired = 3
    }

    public enum UserType
    {
        Admin = 1,
        Builder = 2
    }

    public enum FloorType
    {
        [Description("First Floor")]
        floor1 = 1,
        [Description("Second Floor")]
        floor2 = 2,
        [Description("Third Floor")]
        floor3 = 3,
        [Description("Fourth Floor")]
        floor4 = 4,
        [Description("Fifth Floor")]
        floor5 = 5
    }

    public enum ReferralType
    {
        Internet = 1,
        Realtor = 2,
        Signs = 3,
        Print = 4,
        Referral = 5,
        Media = 6
    }

    public enum ReferralValue
    {
        [Description("On-line Sales Counselor Appt.")]
        OnlineSalesCounselor = 1,

        [Description("Individual Search")]
        IndividualSearch = 2,

        [Description("Name")]
        Name = 3,

        [Description("Office")]
        Office = 4,

        [Description("Billboards")]
        BillBoards = 5,

        [Description("Street Directionals")]
        StreetDirectional = 6,

        [Description("Saw Model while driving by")]
        SawModelwhiledrivingby = 7,

        [Description("Paper")]
        Paper = 8,

        [Description("Magazine")]
        Magazine = 9,

        [Description("Mailer")]
        Mailer = 10,

        [Description("Name")]
        ReferralName = 11,

        [Description("Radio")]
        Radio = 12,

        [Description("TV")]
        TV = 13,

        [Description("Other")]
        Other = 14
    }

    public enum LotStatus
    {
        Available = 1,
        Reserved = 2,
        Sold = 3
    }
}
