﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class InviteUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string EmailToken { get; set; }
        public int GameId { get; set; }
        public short InviteVerified { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual GamePlan Game { get; set; }
    }
}
