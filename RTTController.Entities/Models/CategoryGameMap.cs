﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class CategoryGameMap
    {
        public CategoryGameMap()
        {
            GamePlanCategoryValue = new HashSet<GamePlanCategoryValue>();
        }

        public int Id { get; set; }
        public int? CategoryId { get; set; }
        public int? GameId { get; set; }
        public int? SubCatId { get; set; }
        public int? Sequence { get; set; }
        public int? ThemeId { get; set; }

        public virtual Category Category { get; set; }
        public virtual GamePlan Game { get; set; }
        public virtual SubCategory SubCat { get; set; }
        public virtual Theme Theme { get; set; }
        public virtual ICollection<GamePlanCategoryValue> GamePlanCategoryValue { get; set; }
    }
}
