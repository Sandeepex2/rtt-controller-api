﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class GamePlanImage
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int FloorType { get; set; }
        public string ImageUrl { get; set; }

        public virtual GamePlan Game { get; set; }
    }
}
