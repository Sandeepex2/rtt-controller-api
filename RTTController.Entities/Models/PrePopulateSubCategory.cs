﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class PrePopulateSubCategory
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string SubCategoryName { get; set; }
    }
}
