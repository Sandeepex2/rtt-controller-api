﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class User
    {
        public User()
        {
            GameLead = new HashSet<GameLead>();
            GameUserRelationship = new HashSet<GameUserRelationship>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int UserType { get; set; }
        public int Status { get; set; }
        public short PasswordChangeRequired { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public short? EmailVerified { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string DeviceToken { get; set; }

        public virtual ICollection<GameLead> GameLead { get; set; }
        public virtual ICollection<GameUserRelationship> GameUserRelationship { get; set; }
    }
}
