﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class GameLead
    {
        public GameLead()
        {
            GameLeadReferralSources = new HashSet<GameLeadReferralSources>();
        }

        public int Id { get; set; }
        public int GameId { get; set; }
        public int ReferUserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Photo { get; set; }
        public string PhotoType { get; set; }

        public virtual GamePlan Game { get; set; }
        public virtual User ReferUser { get; set; }
        public virtual ICollection<GameLeadReferralSources> GameLeadReferralSources { get; set; }
    }
}
