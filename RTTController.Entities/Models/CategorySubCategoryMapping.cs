﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class CategorySubCategoryMapping
    {
        public int Id { get; set; }
        public int? CatId { get; set; }
        public int? SubCatId { get; set; }

        public virtual Category Cat { get; set; }
        public virtual SubCategory SubCat { get; set; }
    }
}
