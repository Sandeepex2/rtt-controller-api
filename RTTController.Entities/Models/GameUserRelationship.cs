﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class GameUserRelationship
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }

        public virtual GamePlan Game { get; set; }
        public virtual User User { get; set; }
    }
}
