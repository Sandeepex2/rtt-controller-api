﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class UserEmailVerification
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public short IsVerified { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
