﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class SettingOptions
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? SettingsId { get; set; }

        public virtual Settings Settings { get; set; }
    }
}
