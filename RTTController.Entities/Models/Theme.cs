﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class Theme
    {
        public Theme()
        {
            CategoryGameMap = new HashSet<CategoryGameMap>();
        }

        public int Id { get; set; }
        public int? GameId { get; set; }
        public string Name { get; set; }
        public int? CategoryId { get; set; }

        public virtual GamePlan Game { get; set; }
        public virtual ICollection<CategoryGameMap> CategoryGameMap { get; set; }
    }
}
