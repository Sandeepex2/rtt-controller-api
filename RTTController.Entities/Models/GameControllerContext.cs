﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RTTController.Entities.Models
{
    public partial class GameControllerContext : DbContext
    {
        public GameControllerContext()
        {
        }

        public GameControllerContext(DbContextOptions<GameControllerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Builder> Builder { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<CategoryGameMap> CategoryGameMap { get; set; }
        public virtual DbSet<CategorySubCategoryMapping> CategorySubCategoryMapping { get; set; }
        public virtual DbSet<GameLead> GameLead { get; set; }
        public virtual DbSet<GameLeadReferralSources> GameLeadReferralSources { get; set; }
        public virtual DbSet<GamePlan> GamePlan { get; set; }
        public virtual DbSet<GamePlanCategoryValue> GamePlanCategoryValue { get; set; }
        public virtual DbSet<GamePlanImage> GamePlanImage { get; set; }
        public virtual DbSet<GameUserRelationship> GameUserRelationship { get; set; }
        public virtual DbSet<InviteUser> InviteUser { get; set; }
        public virtual DbSet<PrePopulateSubCategory> PrePopulateSubCategory { get; set; }
        public virtual DbSet<SettingOptions> SettingOptions { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<SubCategory> SubCategory { get; set; }
        public virtual DbSet<Theme> Theme { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserEmailVerification> UserEmailVerification { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL("server=172.17.1.60;port=3306;user=msql_dev;password=Dev@1234;database=GameController;CharSet=utf8mb4;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Builder>(entity =>
            {
                entity.ToTable("Builder", "GameController");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BdxpartnerId)
                    .HasColumnName("BDXPartnerId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.LogoUrl)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category", "GameController");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EventCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CategoryGameMap>(entity =>
            {
                entity.ToTable("CategoryGameMap", "GameController");

                entity.HasIndex(e => e.CategoryId)
                    .HasName("FK_CategoryGameMap_Category");

                entity.HasIndex(e => e.GameId)
                    .HasName("FK_CategoryGameMap_GamePlan");

                entity.HasIndex(e => e.SubCatId)
                    .HasName("FK_CategoryGameMap_SubCategory");

                entity.HasIndex(e => e.ThemeId)
                    .HasName("FK_CategoryGameMap_Theme");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("CategoryID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GameId)
                    .HasColumnName("GameID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SubCatId).HasColumnType("int(11)");

                entity.Property(e => e.ThemeId).HasColumnType("int(11)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.CategoryGameMap)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategoryGameMap_Category");

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.CategoryGameMap)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategoryGameMap_GamePlan");

                entity.HasOne(d => d.SubCat)
                    .WithMany(p => p.CategoryGameMap)
                    .HasForeignKey(d => d.SubCatId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategoryGameMap_SubCategory");

                entity.HasOne(d => d.Theme)
                    .WithMany(p => p.CategoryGameMap)
                    .HasForeignKey(d => d.ThemeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategoryGameMap_Theme");
            });

            modelBuilder.Entity<CategorySubCategoryMapping>(entity =>
            {
                entity.ToTable("CategorySubCategoryMapping", "GameController");

                entity.HasIndex(e => e.CatId)
                    .HasName("FK_CategorySubCategoryMapping_CategorySubCategoryMapping");

                entity.HasIndex(e => e.SubCatId)
                    .HasName("FK_CategorySubCategoryMapping_SubCategory");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CatId).HasColumnType("int(11)");

                entity.Property(e => e.SubCatId).HasColumnType("int(11)");

                entity.HasOne(d => d.Cat)
                    .WithMany(p => p.CategorySubCategoryMapping)
                    .HasForeignKey(d => d.CatId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategorySubCategoryMapping_CategorySubCategoryMapping");

                entity.HasOne(d => d.SubCat)
                    .WithMany(p => p.CategorySubCategoryMapping)
                    .HasForeignKey(d => d.SubCatId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategorySubCategoryMapping_SubCategory");
            });

            modelBuilder.Entity<GameLead>(entity =>
            {
                entity.ToTable("GameLead", "GameController");

                entity.HasIndex(e => e.GameId)
                    .HasName("FK_GameLead_GamePlan");

                entity.HasIndex(e => e.ReferUserId)
                    .HasName("FK_GameLead_User");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Address)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GameId).HasColumnType("int(11)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Photo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhotoType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ReferUserId).HasColumnType("int(11)");

                entity.Property(e => e.State).HasColumnType("char(2)");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.GameLead)
                    .HasForeignKey(d => d.GameId)
                    .HasConstraintName("FK_GameLead_GamePlan");

                entity.HasOne(d => d.ReferUser)
                    .WithMany(p => p.GameLead)
                    .HasForeignKey(d => d.ReferUserId)
                    .HasConstraintName("FK_GameLead_User");
            });

            modelBuilder.Entity<GameLeadReferralSources>(entity =>
            {
                entity.ToTable("GameLeadReferralSources", "GameController");

                entity.HasIndex(e => e.LeadId)
                    .HasName("FK_GameLeadReferralSources_GameLeadReferralSources");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LeadId)
                    .HasColumnName("LeadID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ReferType).HasColumnType("int(11)");

                entity.Property(e => e.ReferValue).HasColumnType("int(11)");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.GameLeadReferralSources)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_GameLeadReferralSources_GameLeadReferralSources");
            });

            modelBuilder.Entity<GamePlan>(entity =>
            {
                entity.ToTable("GamePlan", "GameController");

                entity.HasIndex(e => e.BuilderId)
                    .HasName("FK_GamePlan_Builder");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.BdxcommunityId)
                    .HasColumnName("BDXCommunityId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BuilderId)
                    .HasColumnName("BuilderID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GameVersion).HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Builder)
                    .WithMany(p => p.GamePlan)
                    .HasForeignKey(d => d.BuilderId)
                    .HasConstraintName("FK_GamePlan_Builder");
            });

            modelBuilder.Entity<GamePlanCategoryValue>(entity =>
            {
                entity.ToTable("GamePlanCategoryValue", "GameController");

                entity.HasIndex(e => e.GameCategoryMapId)
                    .HasName("FK_GamePlanCategoryValue_CategoryGameMap");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EventCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GameCategoryMapId)
                    .HasColumnName("GameCategoryMapID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LayerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GameCategoryMap)
                    .WithMany(p => p.GamePlanCategoryValue)
                    .HasForeignKey(d => d.GameCategoryMapId)
                    .HasConstraintName("FK_GamePlanCategoryValue_CategoryGameMap");
            });

            modelBuilder.Entity<GamePlanImage>(entity =>
            {
                entity.ToTable("GamePlanImage", "GameController");

                entity.HasIndex(e => e.GameId)
                    .HasName("FK_GamePlanImage_GamePlan");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FloorType).HasColumnType("int(11)");

                entity.Property(e => e.GameId).HasColumnType("int(11)");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.GamePlanImage)
                    .HasForeignKey(d => d.GameId)
                    .HasConstraintName("FK_GamePlanImage_GamePlan");
            });

            modelBuilder.Entity<GameUserRelationship>(entity =>
            {
                entity.ToTable("GameUserRelationship", "GameController");

                entity.HasIndex(e => e.GameId)
                    .HasName("FK_GameUserRelationship_GamePlan");

                entity.HasIndex(e => e.UserId)
                    .HasName("FK_GameUserRelationship_User");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GameId)
                    .HasColumnName("GameID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.GameUserRelationship)
                    .HasForeignKey(d => d.GameId)
                    .HasConstraintName("FK_GameUserRelationship_GamePlan");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.GameUserRelationship)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_GameUserRelationship_User");
            });

            modelBuilder.Entity<InviteUser>(entity =>
            {
                entity.ToTable("InviteUser", "GameController");

                entity.HasIndex(e => e.GameId)
                    .HasName("FK_InviteUser_GamePlan");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailToken)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GameId).HasColumnType("int(11)");

                entity.Property(e => e.InviteVerified).HasColumnType("bit(1)");

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.InviteUser)
                    .HasForeignKey(d => d.GameId)
                    .HasConstraintName("FK_InviteUser_GamePlan");
            });

            modelBuilder.Entity<PrePopulateSubCategory>(entity =>
            {
                entity.ToTable("PrePopulateSubCategory", "GameController");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CategoryId).HasColumnType("int(11)");

                entity.Property(e => e.SubCategoryName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SettingOptions>(entity =>
            {
                entity.ToTable("SettingOptions", "GameController");

                entity.HasIndex(e => e.SettingsId)
                    .HasName("FK_SettingOptions_Settings_idx");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SettingsId).HasColumnType("int(11)");

                entity.HasOne(d => d.Settings)
                    .WithMany(p => p.SettingOptions)
                    .HasForeignKey(d => d.SettingsId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_SettingOptions_Settings");
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.ToTable("Settings", "GameController");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SubCategory>(entity =>
            {
                entity.ToTable("SubCategory", "GameController");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Theme>(entity =>
            {
                entity.ToTable("Theme", "GameController");

                entity.HasIndex(e => e.GameId)
                    .HasName("FK_Theme_GamePlan");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CategoryId).HasColumnType("int(11)");

                entity.Property(e => e.GameId).HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.Theme)
                    .HasForeignKey(d => d.GameId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Theme_GamePlan");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User", "GameController");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeviceToken)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailVerified).HasColumnType("bit(1)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordChangeRequired).HasColumnType("bit(1)");

                entity.Property(e => e.PasswordSalt)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("int(11)");

                entity.Property(e => e.UserType).HasColumnType("int(11)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserEmailVerification>(entity =>
            {
                entity.ToTable("UserEmailVerification", "GameController");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsVerified).HasColumnType("bit(1)");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });
        }
    }
}
