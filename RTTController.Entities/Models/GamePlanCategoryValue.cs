﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class GamePlanCategoryValue
    {
        public int Id { get; set; }
        public int GameCategoryMapId { get; set; }
        public string EventCode { get; set; }
        public string ImageUrl { get; set; }
        public string LayerName { get; set; }

        public virtual CategoryGameMap GameCategoryMap { get; set; }
    }
}
