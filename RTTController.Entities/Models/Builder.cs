﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class Builder
    {
        public Builder()
        {
            GamePlan = new HashSet<GamePlan>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public short? IsActive { get; set; }
        public string LogoUrl { get; set; }
        public string BdxpartnerId { get; set; }

        public virtual ICollection<GamePlan> GamePlan { get; set; }
    }
}
