﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class Category
    {
        public Category()
        {
            CategoryGameMap = new HashSet<CategoryGameMap>();
            CategorySubCategoryMapping = new HashSet<CategorySubCategoryMapping>();
        }

        public int Id { get; set; }
        public string EventCode { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<CategoryGameMap> CategoryGameMap { get; set; }
        public virtual ICollection<CategorySubCategoryMapping> CategorySubCategoryMapping { get; set; }
    }
}
