﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class GameLeadReferralSources
    {
        public int Id { get; set; }
        public int LeadId { get; set; }
        public int ReferType { get; set; }
        public int ReferValue { get; set; }
        public string Description { get; set; }

        public virtual GameLead Lead { get; set; }
    }
}
