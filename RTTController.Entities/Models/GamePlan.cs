﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class GamePlan
    {
        public GamePlan()
        {
            CategoryGameMap = new HashSet<CategoryGameMap>();
            GameLead = new HashSet<GameLead>();
            GamePlanImage = new HashSet<GamePlanImage>();
            GameUserRelationship = new HashSet<GameUserRelationship>();
            InviteUser = new HashSet<InviteUser>();
            Theme = new HashSet<Theme>();
        }

        public int Id { get; set; }
        public int BuilderId { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? GameVersion { get; set; }
        public string BdxcommunityId { get; set; }

        public virtual Builder Builder { get; set; }
        public virtual ICollection<CategoryGameMap> CategoryGameMap { get; set; }
        public virtual ICollection<GameLead> GameLead { get; set; }
        public virtual ICollection<GamePlanImage> GamePlanImage { get; set; }
        public virtual ICollection<GameUserRelationship> GameUserRelationship { get; set; }
        public virtual ICollection<InviteUser> InviteUser { get; set; }
        public virtual ICollection<Theme> Theme { get; set; }
    }
}
