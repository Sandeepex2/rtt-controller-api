﻿using System;
using System.Collections.Generic;

namespace RTTController.Entities.Models
{
    public partial class Settings
    {
        public Settings()
        {
            SettingOptions = new HashSet<SettingOptions>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<SettingOptions> SettingOptions { get; set; }
    }
}
