﻿using log4net;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using RTTController.Repository.UnitOfWork;
using System.Linq;
using RTTController.Entities.Models;
using System.Globalization;

namespace RTTController.Services.Services
{
    public class ThemeService : IThemeService
    {
        #region Private Members
        private readonly ILog _logger = LogManager.GetLogger(typeof(BuilderService));
        private readonly SharedUnit _sharedUnit;

        #endregion

        #region Constructor
        public ThemeService(SharedUnit sharedUnit)
        {
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");
        }
        #endregion


        public ThemeResponse Create(ThemeRequest theme)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            var entity = new Theme
            {
                Name = textInfo.ToTitleCase(theme.Name.Trim()),
                GameId = theme.GameId,
                CategoryId = theme.CategoryId
            };
            var data = _sharedUnit.Theme.Add(entity);
            _sharedUnit.SaveChanges();

            return new ThemeResponse()
            {
                Id = data.Id,
                Name = data.Name,
                GameId = data.GameId,                
                CategoryId = data.CategoryId
            };
        }

        public bool Delete(int Id)
        {
            _sharedUnit.Theme.Delete(Id);
            _sharedUnit.SaveChanges();

            return true;
        }

        public IEnumerable<ThemeResponse> Get()
        {
            return _sharedUnit.Theme.Find(o => o.Id > 0)
            .Select(o => new ThemeResponse
            {
                Id = o.Id,
                Name = o.Name,
                GameId = o.GameId,
                CategoryId = o.CategoryId
            }).ToList();
        }

        public IEnumerable<ThemeResponse> GetByGameId(int Id)
        {
            return _sharedUnit.Theme.Find(o => o.GameId == Id)
                .Select(o => new ThemeResponse
                {
                    Id = o.Id,
                    Name = o.Name,
                    GameId = o.GameId,
                    CategoryId = o.CategoryId
                }).ToList();
        }

        public ThemeResponse Update(ThemeRequest theme)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            var entity = _sharedUnit.Theme.Find(theme.Id);

            entity.Name = textInfo.ToTitleCase(theme.Name.Trim());

            var data = _sharedUnit.Theme.Update(entity);
            _sharedUnit.SaveChanges();

            return new ThemeResponse()
            {
                Id = data.Id,
                Name = data.Name,
                GameId = data.GameId,
                CategoryId = data.CategoryId                
            };            
        }
    }
}
