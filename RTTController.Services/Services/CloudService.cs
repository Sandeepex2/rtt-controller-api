﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RTTController.Contracts.Requests;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Amazon.S3;
using Amazon.S3.Model;
using RTTController.Contracts.Responses;
using System.IO;
using RTTController.Contracts;

namespace RTTController.Services.Services
{
    public class CloudService : ICloudService
    {
        #region private members
        private ILog _logger = LogManager.GetLogger(typeof(CloudService));
        private readonly AppSettings _appSettings;
        private readonly UserUnit _usersUnit;
        private readonly SharedUnit _sharedUnit;
        #endregion

        #region Constructor
        public CloudService(IOptions<AppSettings> appSettings, UserUnit userUnit, SharedUnit sharedUnit)
        {
            _appSettings = appSettings.Value;
            _usersUnit = userUnit ?? throw new ArgumentNullException("UserUnit reference cannot be passed null");
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");
        }

        #endregion

        #region Service Methods
        public async Task<UploadPhotoResponse> UploadFile(IFormFile file)
        {
            var amazonS3Config = new AmazonS3Config();
            amazonS3Config.RegionEndpoint = Amazon.RegionEndpoint.USEast1; // use your region endpoint
            var client = new AmazonS3Client(_appSettings.AWSAccessKey, _appSettings.AWSSecretKey, amazonS3Config);

            byte[] fileBytes = new Byte[file.Length]; //get byte code from file
            file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));
            var fileName = Guid.NewGuid() + file.FileName; //unique filename

            PutObjectResponse response = null;

            using (var stream = new MemoryStream(fileBytes))
            {
                var request = new PutObjectRequest
                {
                    BucketName = _appSettings.AWSS3Bucket,
                    Key = fileName,
                    InputStream = stream,
                    CannedACL = S3CannedACL.PublicRead
                };

                response = await client.PutObjectAsync(request);

                _logger.Info("AWS File Upload response" + JsonConvert.SerializeObject(response));
            };

            return new UploadPhotoResponse
            {
                FileName = fileName,
                Status = response.HttpStatusCode
            };
        }

        /// <summary>
        /// Delete an object from AWS s3 bucket
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAWSFile(string keyName)
        {
            if (string.IsNullOrEmpty(keyName))
                return true;

            var amazonS3Config = new AmazonS3Config();
            amazonS3Config.RegionEndpoint = Amazon.RegionEndpoint.USEast1; // use your region endpoint
            var client = new AmazonS3Client(_appSettings.AWSAccessKey, _appSettings.AWSSecretKey, amazonS3Config);

            var deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = _appSettings.AWSS3Bucket,
                Key = keyName
            };
            var response = await client.DeleteObjectAsync(deleteObjectRequest);
            if (response.HttpStatusCode == HttpStatusCode.NoContent)
                return true;
            else
                return false;
        }

        public string GetAWSFile(string keyName)
        {
            if (string.IsNullOrEmpty(keyName))
                return "";

            // return "https://s3.amazonaws.com/" + _appSettings.AWSS3Bucket + "/" + keyName;

            string env = _appSettings.AWSS3Bucket.Substring(_appSettings.AWSS3Bucket.IndexOf('/') + 1);

            return "https://gamecontroller.s3.amazonaws.com/" + env + "/" + keyName;
        }

        public async Task<bool> SendMail(EmailRequest request)
        {
            using (var ses = new AmazonSimpleEmailServiceClient(_appSettings.AWSAccessKey, _appSettings.AWSSecretKey, RegionEndpoint.USEast1))
            {
                var sendResult = await ses.SendEmailAsync(new SendEmailRequest
                {
                    Source = request.Sender,
                    Destination = new Destination
                    {
                        ToAddresses =
                        new List<string> { request.Recipient }
                    },
                    Message = new Message
                    {
                        Subject = new Content(request.Subject),
                        Body = new Body
                        {
                            Html = new Content(request.HtmlBody),
                        }
                    }
                });
                _logger.Info("Email Response " + JsonConvert.SerializeObject(sendResult));
                return sendResult.HttpStatusCode == HttpStatusCode.OK;
            }
        }

        public bool IsUserExists(string email)
        {
            return _usersUnit.User.Find(u => u.Email.ToLower() == email.ToLower()).Count() > 0 ? true : false;
        }

        public bool IsUserExists(int id)
        {
            return _usersUnit.User.Find(u => u.UserId == id).Count() > 0 ? true : false;
        }

        public bool IsGameExists(int id)
        {
            return _usersUnit.GamePlan.Find(u => u.Id == id).Count() > 0 ? true : false;
        }

        public bool IsGameExists(string name)
        {
            return _usersUnit.GamePlan.Find(u => u.Name.ToLower() == name.ToLower()).Count() > 0 ? true : false;
        }

        public bool IsGameExists(string name, int builderId)
        {
            return _usersUnit.GamePlan.Find(u => u.Name.ToLower() == name.ToLower() && u.BuilderId == builderId).Count() > 0 ? true : false;
        }

        public bool IsBuilderExists(string name)
        {
            return _sharedUnit.Builder.Find(x => x.Name.ToLower() == name.ToLower()).Count() > 0 ? true : false;
        }

        public bool IsBuilderExists(int id)
        {
            return _sharedUnit.Builder.Find(x => x.Id == id).Count() > 0 ? true : false;
        }

        public bool IsCategoryExists(string name)
        {
            return _sharedUnit.Category.Find(x => x.Name.ToLower() == name.ToLower()).Count() > 0 ? true : false;
        }

        public bool IsCategoryExists(int id)
        {
            return _sharedUnit.Category.Find(x => x.Id == id).Count() > 0 ? true : false;
        }

        public bool IsSubCategoryExists(string name)
        {
            return _sharedUnit.SubCategory.Find(x => x.Name.ToLower() == name.ToLower()).Count() > 0 ? true : false;
        }

        public bool IsSubCategoryExists(int id)
        {
            return _sharedUnit.SubCategory.Find(x => x.Id == id).Count() > 0 ? true : false;
        }

        public bool IsSubCategoryExists(int id, string name)
        {
            return _sharedUnit.SubCategory.Find(x => x.Id != id && x.Name == name).Count() > 0 ? true : false;
        }

        public bool IsSubCategoryExistsInGame(string name, int categoryId, int gameId , int? themeId)
        {
            var subcategory = _sharedUnit.SubCategory.Find(x => x.Name.ToLower() == name.ToLower()).FirstOrDefault();
            if (subcategory == null)
                return false;

            return _sharedUnit.CategoryGameMap.Find(x => x.CategoryId == categoryId && x.GameId == gameId && x.SubCatId == subcategory.Id && x.ThemeId == themeId).Count() > 0 ? true : false;
        }

        public bool IsEventCodeExistsInSubCategory(int gameId, int categoryId, int subcategoryId, string eventCode , int? themid)
        {
            return _sharedUnit.GamePlanCategoryValue.Find(g => g.EventCode == eventCode && g.GameCategoryMap.GameId == gameId
            && g.GameCategoryMap.CategoryId == categoryId && g.GameCategoryMap.SubCatId == subcategoryId && Convert.ToInt32(g.GameCategoryMap.ThemeId) == Convert.ToInt32(themid)).Count() > 0 ? true : false;
        }

        public bool IsBuilderExists(int id, string name)
        {
            return _sharedUnit.Builder.Find(x => x.Name.ToLower() == name.ToLower() && x.Id != id).Count() > 0 ? true : false;
        }       

        public int GetGameId(int id, RequestType type)
        {
            var gameId = 0;
            switch (type)
            {
                case RequestType.PlanSvg:
                    gameId = _sharedUnit.GamePlanImage.Find(id).GameId;
                    break;
                case RequestType.SubCategory:
                    gameId = _sharedUnit.CategoryGameMap.Find(id).GameId ?? 0;
                    break;
                case RequestType.Images:
                    gameId = _sharedUnit.GamePlanCategoryValue.Find(id).GameCategoryMap.GameId ?? 0;
                    break;
                default:
                    break;
            }
            return gameId;
        }

        public bool UpdateGameVersion(int gameId)
        {
            var gamePlan = _sharedUnit.Game.Find(gameId);
            gamePlan.GameVersion = gamePlan.GameVersion + 1;

            _sharedUnit.Game.Update(gamePlan);
            _sharedUnit.SaveChanges();

            return true;
        }

        #endregion
    }
}
