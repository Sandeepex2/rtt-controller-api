﻿using log4net;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using RTTController.Entities.Models;
using System.Linq;
using System.Globalization;

namespace RTTController.Services.Services
{
    public class SubCategoryService : ISubCategoryService
    {
        #region Private Members
        private readonly ILog _logger = LogManager.GetLogger(typeof(CategoryService));
        private readonly SharedUnit _sharedUnit;

        #endregion

        #region Constructor
        public SubCategoryService(SharedUnit sharedUnit)
        {
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");
        }

        #endregion

        #region Service Methods
        public SubCategoryResponse Create(SubCategoryRequest request)
        {
            var category = _sharedUnit.Category.Find(request.CategoryId);
            SubCategory subCategory = _sharedUnit.SubCategory.Find(o => o.Name == request.Name).FirstOrDefault();

            if (subCategory == null)
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                subCategory = new SubCategory
                {                    
                    Name = textInfo.ToTitleCase(request.Name.Trim().ToLower())
                };

                CategorySubCategoryMapping categorySubCategory = new CategorySubCategoryMapping
                {
                    Cat = category,
                    SubCat = subCategory
                };

                subCategory.CategorySubCategoryMapping.Add(categorySubCategory);

                CategoryGameMap categoryGameMap = new CategoryGameMap
                {
                    Category = category,
                    SubCat = subCategory,
                    GameId = request.GameId,
                    ThemeId = request.ThemeId,
                    Sequence = _sharedUnit.CategoryGameMap
                        .Find(x => x.GameId == request.GameId && x.CategoryId == request.CategoryId)
                        .Max(m => m.Sequence) + 1
                };
                subCategory.CategoryGameMap.Add(categoryGameMap);


                var data = _sharedUnit.SubCategory.Add(subCategory);
                _sharedUnit.SaveChanges();

                return new SubCategoryResponse
                {
                    Id = data.Id,
                    Name = data.Name.Trim()
                };
            }
            else
            {
                CategoryGameMap categoryGameMap = new CategoryGameMap
                {
                    Category = category,
                    SubCat = subCategory,
                    GameId = request.GameId,
                    ThemeId = request.ThemeId,
                    Sequence = _sharedUnit.CategoryGameMap
                    .Find(x => x.GameId == request.GameId && x.CategoryId == request.CategoryId)
                    .Max(m => m.Sequence) + 1
                };
                subCategory.CategoryGameMap.Add(categoryGameMap);

                var data = _sharedUnit.SubCategory.Update(subCategory);
                _sharedUnit.SaveChanges();

                return new SubCategoryResponse
                {
                    Id = data.Id,
                    Name = data.Name.Trim()
                };
            }           
        }

        public IEnumerable<SubCategoryResponse> Get()
        {
            var subcategories = _sharedUnit.SubCategory.Find(x => x.Id != 0).ToList();

            return subcategories.Select(s => new SubCategoryResponse
            {
                Id = s.Id,
                Name = s.Name
            }).ToList();
        }

        public SubCategoryResponse GetById(int id)
        {
            var subcategory = _sharedUnit.SubCategory.Find(id);

            return new SubCategoryResponse
            {
                Id = subcategory.Id,
                Name = subcategory.Name
            };

        }

        public SubCategoryResponse Update(SubCategoryUpdateRequest request)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            var entity = _sharedUnit.SubCategory.Find(request.Id);
            entity.Name = textInfo.ToTitleCase(request.Name.Trim().ToLower());            

            entity = _sharedUnit.SubCategory.Update(entity);
            _sharedUnit.SaveChanges();

            return new SubCategoryResponse
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
        #endregion
    }
}
