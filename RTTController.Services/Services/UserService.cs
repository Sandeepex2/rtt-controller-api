﻿using log4net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Entities.Enums;
using RTTController.Entities.Models;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace RTTController.Services.Services
{
    public class UserService : IUserService
    {
        #region Private members
        private readonly UserUnit _userUnit;
        private readonly ILog _logger = LogManager.GetLogger(typeof(UserService));
        private readonly ICloudService _cloudService;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _host;
        #endregion

        #region Constructor
        /// <summary>
        /// Construct UserService Object
        /// </summary>
        /// <param name="userUnit"></param>
        /// <param name="appSettings"></param>
        /// <param name="cloudService"></param>
        public UserService(UserUnit userUnit, IOptions<AppSettings> appSettings, ICloudService cloudService, IHostingEnvironment host)
        {
            _userUnit = userUnit ?? throw new ArgumentNullException("UserUnit reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
            _host = host;
        }

        #endregion

        #region Service Methods

        /// <summary>
        /// Update User Token
        /// </summary>
        /// <returns></returns>
        public UserResponse UpdateDeviceToken(DeviceTokenRequest request)
        {
            var user = _userUnit.User.Find(request.UserId);

            if (user == null)
                return null;

            user.DeviceToken = request.DeviceToken;
            _userUnit.User.Update(user);

            return GetUserById(user.UserId);
        }


        /// <summary>
        /// Authenticate User
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public AuthenticateResponse Authenticate(AuthenticateRequest request)
        {
            if (string.IsNullOrEmpty(request.Username) || string.IsNullOrEmpty(request.Password))
                return null;

            var authUser = _userUnit.User.Find(x => x.Username.ToLower() == request.Username.ToLower()).FirstOrDefault();

            if (authUser == null)
                return null;

            if (!Helper.VerifyPassword(request.Password, authUser.Password, authUser.PasswordSalt))
                return null;

            var response = GetUserById(authUser.UserId);
            response.Token = CreateToken(response);

            return new AuthenticateResponse
            {
                AccessToken = response.Token,
                Username = response.Username,
                UserType = response.UserType,
                UserId = response.UserId,
                Expired = DateTime.UtcNow.AddDays(1).ToString("r")
                //Expired = DateTime.UtcNow.AddMinutes(2).ToString("r")
            };

        }

        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="userRequest"></param>
        /// <returns></returns>
        public async Task<UserResponse> CreateUser(UserRequest userRequest)
        {
            UserResponse response = null;
            string passwordHash = "", passwordSalt = "";

            var gamePlan = _userUnit.GamePlan.Find(x => x.Id == userRequest.GameId).FirstOrDefault();

            Helper.GenerateSaltedHash(userRequest.Password, out passwordHash, out passwordSalt);

            User userEntity = new User
            {
                Username = userRequest.UserName.Trim(),
                FirstName = userRequest.FirstName.Trim(),
                LastName = userRequest.LastName.Trim(),
                UserType = userRequest.UserType == 0 ? (int)UserType.Builder : (int)UserType.Admin,
                Email = userRequest.Email.Trim(),
                Status = (int)UserStatus.Active,
                Password = passwordHash,
                PasswordSalt = passwordSalt,
                CreatedBy = "RTTController.API",
                CreatedDate = DateTime.UtcNow
            };

            using (var trans = _userUnit.BeginTransaction())
            {
                try
                {
                    var user = _userUnit.User.Add(userEntity);
                    _userUnit.SaveChanges();

                    if (user != null)
                    {
                        GameUserRelationship gameUserRelationship = new GameUserRelationship
                        {
                            User = user,
                            Game = gamePlan
                        };

                        _userUnit.GameUserRelationship.Add(gameUserRelationship);
                        _userUnit.SaveChanges();
                    }

                    var inviteEntity = _userUnit.InviteUser.Find(x => x.Email.ToLower() == userRequest.Email.ToLower() && x.EmailToken.ToLower() == userRequest.InviteToken.ToLower()).FirstOrDefault();
                    if (inviteEntity != null)
                    {
                        inviteEntity.InviteVerified = 1;
                        _userUnit.InviteUser.Update(inviteEntity);
                        _userUnit.SaveChanges();
                    }

                    response = UserMapper(user);

                    trans.Commit();
                }

                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }

            }

            #region Send Email

            var path = _host.ContentRootPath;

            var emailTemplate = System.IO.File.ReadAllText(path + "/EmailTemplate/ConfirmationEmailRemplate.html");
            emailTemplate = emailTemplate.Replace("{name}", response.LastName);

            //// Send email 
            EmailRequest emailRequest = new EmailRequest
            {
                Sender = _appSettings.SenderAddress,
                Recipient = response.Email,
                Subject = "Game Controller Welcome Email",
                HtmlBody = emailTemplate
            };

            var result = await _cloudService.SendMail(emailRequest);

            #endregion

            return response;
        }

        /// <summary>
        ///  Retrieves all Users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserResponse> GetAllUsers()
        {
            var users = _userUnit.User.Find(x => x.UserId != 0).ToList();
            return MappedUsers(users);

        }

        /// <summary>
        /// Retrieves the User by Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserResponse GetUserByEmail(string email)
        {
            var userData = _userUnit.User.Find(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();
            return UserMapper(userData);
        }

        /// <summary>
        ///  Retrieves the User by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserResponse GetUserById(int userId)
        {

            var userData = _userUnit.User.Find(userId);
            return UserMapper(userData);
        }

        public async Task<bool> InviteUser(InviteGameRequest request)
        {
            bool ret_val = false;

            GameUserRelationship gameUserRelationship = null;

            var path = _host.ContentRootPath;

            var gamePlan = _userUnit.GamePlan.Find(x => x.Id == request.GameId).FirstOrDefault();

            var userData = _userUnit.User.Find(x => x.Email.ToLower() == request.Email.ToLower()).FirstOrDefault();

            if (userData != null)
                gameUserRelationship = userData.GameUserRelationship.Where(x => x.GameId == request.GameId).FirstOrDefault();

            // Game Already invited

            string emailToken = Guid.NewGuid().ToString();

            string inviteLink = _appSettings.BaseUrl + "/register/" + emailToken;

            if (userData != null && gameUserRelationship == null)
            {
                GameUserRelationship gameUserEntity = new GameUserRelationship
                {
                    Game = gamePlan,
                    User = userData
                };

                userData.GameUserRelationship.Add(gameUserEntity);

                var response = _userUnit.User.Update(userData);
                _userUnit.SaveChanges();

                if (response != null)
                {
                    #region Send Email                    

                    var emailTemplate = System.IO.File.ReadAllText(path + "/EmailTemplate/GameEmailTemplate.html");
                    emailTemplate = emailTemplate.Replace("{game}", gamePlan.Name).Replace("{name}", userData.LastName);


                    //// Send email 
                    EmailRequest emailRequest = new EmailRequest
                    {
                        Sender = _appSettings.SenderAddress,
                        Recipient = request.Email,
                        Subject = "Game Controller Invitation",
                        HtmlBody = emailTemplate
                    };

                    ret_val = await _cloudService.SendMail(emailRequest);

                    #endregion
                }

            }
            else
            {
                #region Send Email                

                var emailTemplate = System.IO.File.ReadAllText(path + "/EmailTemplate/InviteEmailTemplate.html");
                emailTemplate = emailTemplate.Replace("{Link}", inviteLink).Replace("{email}", request.Email).Replace("{game}", request.GameName);

                //// Send email 
                EmailRequest emailRequest = new EmailRequest
                {
                    Sender = _appSettings.SenderAddress,
                    Recipient = request.Email,
                    Subject = "Game Controller Invitation",
                    HtmlBody = emailTemplate
                };

                ret_val = await _cloudService.SendMail(emailRequest);

                if (ret_val)
                {
                    InviteUserRequest inviteRequest = new InviteUserRequest
                    {
                        Email = request.Email,
                        EmailToken = Guid.Parse(emailToken),
                        GameId = request.GameId
                    };

                    InviteUserForGame(inviteRequest);
                }

                #endregion
            }

            return ret_val;
        }

        public bool InviteUserForGame(InviteUserRequest request)
        {
            InviteUser inviteUser = new InviteUser
            {
                Email = request.Email,
                EmailToken = request.EmailToken.ToString(),
                GameId = request.GameId,
                CreatedBy = "InviteUser.Service",
                CreatedDate = DateTime.UtcNow,
                //InviteVerified = 1
            };

            var inviteUserResponse = _userUnit.InviteUser.Add(inviteUser);
            _userUnit.SaveChanges();

            return inviteUserResponse != null ? true : false;
        }


        public InviteUserResponse VerifyEmailToken(string emailToken)
        {
            var inviteUserData = _userUnit.InviteUser.Find(x => x.EmailToken == emailToken).FirstOrDefault();

            return new InviteUserResponse
            {
                ID = inviteUserData.Id,
                Email = inviteUserData.Email,
                EmailToken = inviteUserData.EmailToken,
                GameId = inviteUserData.GameId
            };
        }

        public bool IsGameAlreadyAssignedToUser(string email, int gameId)
        {
            GameUserRelationship gameUserRelationship = null;

            var userData = _userUnit.User.Find(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();

            if (userData != null)
                gameUserRelationship = userData.GameUserRelationship.Where(x => x.GameId == gameId).FirstOrDefault();

            return gameUserRelationship != null ? true : false;

        }

        public bool DeleteUserGame(DeleteUserGameRequest request)
        {
            var _entity = _userUnit.GameUserRelationship.Find(x => x.UserId == request.UserID && x.GameId == request.GameID).FirstOrDefault();

            if (_entity != null)
            {
                var response = _userUnit.GameUserRelationship.Delete(_entity.Id);
                _userUnit.SaveChanges();
                return true;
            }
            return false;
        }

        public List<InviteGameUserResponse> GetInviteUserList()
        {
            var _entity = _userUnit.InviteUser.Find(x => x.Id != 0)
                .GroupBy(g => new { g.Email, g.InviteVerified, g.GameId, g.Game.Name })
                .Select(x => new InviteGameUserResponse
                {
                    Email = x.Key.Email,
                    GameID = x.Key.GameId,
                    GameName = x.Key.Name,
                    Status = x.Key.InviteVerified == 0 ? "Pending" : "Accepted",
                    InviteDate = x.Max(s => s.CreatedDate).ToString("f")
                })
                .ToList();
            return _entity;

            // return MapUserGameInvite(_entity);
        }

        #endregion

        #region Private Methods
        private UserResponse UserMapper(User result)
        {
            if (result == null) return null;
            return new UserResponse
            {
                UserId = result.UserId,
                Username = result.Username,
                FirstName = result.FirstName,
                LastName = result.LastName,
                Email = result.Email,
                UserType = EnumHelper.GetEnumValue<UserType>(result.UserType).ToString(),
                Status = EnumHelper.GetEnumValue<UserStatus>(result.Status).ToString(),
                DeviceToken = result.DeviceToken,
                CreatedDate = result.CreatedDate,
                UserGames = result.GameUserRelationship == null ? null : result.GameUserRelationship.Select(s => new UserResponse.UserGame
                {
                    GameId = s.Game.Id,
                    GameName = s.Game.Name,
                    GameVerion = s.Game.GameVersion,
                    GameImamge = s.Game.Builder != null ? _cloudService.GetAWSFile(s.Game.Builder.LogoUrl) : ""
                }).ToList()
            };
        }

        private IEnumerable<UserResponse> MappedUsers(List<User> users)
        {
            return users.Select(user => new UserResponse
            {
                UserId = user.UserId,
                Username = user.Username,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                UserType = EnumHelper.GetEnumValue<UserType>(user.UserType).ToString(),
                Status = EnumHelper.GetEnumValue<UserStatus>(user.Status).ToString(),
                CreatedDate = user.CreatedDate,
                UserGames = user.GameUserRelationship == null ? null : user.GameUserRelationship.Select(s => new UserResponse.UserGame
                {
                    GameId = s.Game.Id,
                    GameName = s.Game.Name,
                    GameVerion = s.Game.GameVersion,
                    GameImamge = s.Game.Builder != null ? _cloudService.GetAWSFile(s.Game.Builder.LogoUrl) : ""
                }).ToList()
            });
        }

        /// <summary>
        /// Create JWT Token
        /// </summary>
        /// <param name="authUser"></param>
        /// <returns></returns>
        private string CreateToken(UserResponse authUser)
        {
            JwtSecurityToken tokenOptions;
            // authentication successful so generate jwt token
            var key = Encoding.ASCII.GetBytes(_appSettings.AudienceSecret);
            var secretKey = new SymmetricSecurityKey(key);
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, authUser.Email),
                new Claim("userId", authUser.UserId.ToString()),
                new Claim("userType", authUser.UserType.ToString()),
                new Claim("userStatus", authUser.Status)
            };

            tokenOptions = new JwtSecurityToken(
                issuer: _appSettings.Issuer,
                audience: _appSettings.AudienceId,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddDays(1),
                signingCredentials: signinCredentials
            );
            return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
        }

        private List<InviteGameUserResponse> MapUserGameInvite(List<InviteUser> inviteUsers)
        {
            if (inviteUsers.Count == 0) return new List<InviteGameUserResponse> { };

            return inviteUsers.Select(s => new InviteGameUserResponse
            {
                GameID = s.GameId,
                Email = s.Email,
                GameName = s.Game.Name,
                Status = s.InviteVerified == 0 ? "Pending" : "Accepted",
                InviteDate = s.CreatedDate.ToString("f")
            }).ToList();
        }

        #endregion
    }
}
