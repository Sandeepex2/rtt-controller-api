﻿using log4net;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Entities.Models;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RTTController.Services.Services
{
    public class CategoryService : ICategoryService
    {
        #region Private Members
        private readonly ILog _logger = LogManager.GetLogger(typeof(CategoryService));
        private readonly SharedUnit _sharedUnit;
        
        #endregion

        #region Constructor
        public CategoryService( SharedUnit sharedUnit)
        {
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");           
        }

        #endregion


        #region Service Methods
        public CategoryResponse Create(CategoryRequest request)
        {
            Category categoryEntity = new Category
            {
                EventCode = request.EventCode.Trim(),
                Name = request.Name.Trim(),
                CreatedBy = "Category:Service",
                CreatedDate = DateTime.UtcNow
            };

            var category = _sharedUnit.Category.Add(categoryEntity);
            _sharedUnit.SaveChanges();

            return MapCategory(category);
        }

        public IEnumerable<CategoryResponse> Get()
        {
            var categories = _sharedUnit.Category.Find(x => x.Id != 0).ToList();
            return MapCategoryCollection(categories);
        }

        public CategoryResponse GetCategory(int catId)
        {
            var category = _sharedUnit.Category.Find(catId);

            return MapCategory(category);
        }
        #endregion

        #region Mapper methods

        private List<CategoryResponse> MapCategoryCollection(List<Category> categories)
        {
            if (categories.Count==0) return new List<CategoryResponse> { };

            return categories.Select(s => new CategoryResponse
            {
                CategoryId = s.Id,
                Name = s.Name,
                EventCode = s.EventCode,
                SubCategories = s.CategorySubCategoryMapping == null ? null : s.CategorySubCategoryMapping.Select(x => new CategoryResponse.SubCategory
                {
                    SubCategoryId = x.SubCat.Id,
                    Name = x.SubCat.Name
                }).ToList(),
                CreatedBy = s.CreatedBy,
                CreatedDate = s.CreatedDate
            }).ToList();
        }

        private CategoryResponse MapCategory(Category category)
        {
            if (category == null) return null;
            return new CategoryResponse
            {
                CategoryId = category.Id,
                EventCode = category.EventCode,
                Name = category.Name,
                SubCategories = category.CategorySubCategoryMapping == null ? null : category.CategorySubCategoryMapping.Select(s => new CategoryResponse.SubCategory
                {
                    SubCategoryId = s.SubCat.Id,
                    Name = s.SubCat.Name
                }).ToList(),
                CreatedBy = category.CreatedBy,
                CreatedDate = category.CreatedDate
            };
        }
        #endregion
    }
}
