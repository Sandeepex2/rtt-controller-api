﻿using log4net;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Entities.Models;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using RTTController.Services.Helpers;
using RTTController.Entities.Enums;

namespace RTTController.Services.Services
{
    public class GameLeadService : IGameLeadService
    {
        #region Private Members
        private readonly GamePlanUnit _gamePlanUnit;
        private readonly SharedUnit _sharedUnit;
        private readonly ICloudService _cloudService;
        private readonly ILog _logger = LogManager.GetLogger(typeof(GameService));
        #endregion

        #region Constructor
        public GameLeadService(GamePlanUnit gamePlanUnit, SharedUnit sharedUnit, ICloudService cloudService)
        {
            _gamePlanUnit = gamePlanUnit ?? throw new ArgumentNullException("GamePlanUnit reference cannot be passed null");
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
        }
        #endregion

        #region Service methods
        public GameUserLead CreateGameLead(GameLeadRequest gameLead)
        {

            GameUserLead response = null;
            GamePlan gamePlan = _gamePlanUnit.GamePlan.Find(gameLead.GameId);

            User referUser = _gamePlanUnit.GameUser.Find(gameLead.UserId);

            GameLead modelGL = new GameLead
            {
                GameId = gameLead.GameId,
                ReferUser = referUser,
                Email = gameLead.Email.Trim(),
                FirstName = gameLead.FirstName.Trim(),
                LastName = gameLead.LastName.Trim(),
                Address = gameLead.Address,
                City = gameLead.City,
                State = gameLead.State,
                ZipCode = gameLead.Zipcode,
                PhoneNumber = gameLead.Phone,
                PhotoType = gameLead.PhotoType,
                Photo = gameLead.Photo,
                CreatedBy = "GamePlan.Service",
                CreatedDate = DateTime.UtcNow,
                GameLeadReferralSources = MapReferralSource(gameLead.ReferralSources)
            };

            gamePlan.GameLead.Add(modelGL);

            var gameData = _gamePlanUnit.GameLead.Add(modelGL);
            _gamePlanUnit.SaveChanges();

            response = MapGameLead(gameData);

            return response;
        }

        public GameLeadResponse GetGameLead(int gameId, int userId)
        {
            GameLeadResponse response = null;

            var gamePlan = _gamePlanUnit.GamePlan.Find(gameId);

            var gameLeads = _gamePlanUnit.GameLead.Find(x => x.ReferUserId == userId && x.GameId == gameId).ToList();

            response = MapGameLeads(gameLeads);

            if (response == null)
            {
                response = new GameLeadResponse
                {
                    GameId = gameId,
                    GameName = gamePlan.Name,
                    GameUserLead = new List<GameUserLead> { }
                };
            }
            return response;
        }

        public GameLeadResponse GetGameLeadByGameId(int gameId)
        {
            var gameLeads = _gamePlanUnit.GameLead.Find(x => x.GameId == gameId).ToList();

            return MapGameLeads(gameLeads);
        }

        public GameLeadResponse GetGameLeadByUserId(int userId)
        {
            var gameLeads = _gamePlanUnit.GameLead.Find(x => x.ReferUserId == userId).ToList();

            return MapGameLeads(gameLeads);
        }

        public GameLeadResponse GetGameLeads()
        {
            var gameLeads = _gamePlanUnit.GameLead.Find(x => x.Id != 0).ToList();

            return MapGameLeads(gameLeads);
        }
        #endregion


        #region Private Methods
        private GameLeadResponse MapGameLeads(List<GameLead> gameLeads)
        {

            if (gameLeads.Count == 0) return null;
            List<GameUserLead> gameUserLeads = new List<GameUserLead>();
            GameLeadResponse gameLeadResponse = null;

            gameLeadResponse = new GameLeadResponse();

            gameLeadResponse.GameId = gameLeads.FirstOrDefault().GameId;
            gameLeadResponse.GameName = gameLeads.FirstOrDefault().Game.Name;

            foreach (var leadData in gameLeads)
            {
                var ResourceTypeNameList = leadData.GameLeadReferralSources.GroupBy(r => r.ReferType).Select(g => new { ReferType = g.Key, ReferData = g });

                GameUserLead lead = new GameUserLead
                {
                    ID = leadData.Id,
                    GameId = leadData.GameId,
                    ReferUser = leadData.ReferUser.FirstName + " " + leadData.ReferUser.LastName,
                    FirstName = leadData.FirstName,
                    LastName = leadData.LastName,
                    Email = leadData.Email,
                    Address = leadData.Address,
                    City = leadData.City,
                    State = leadData.State,
                    Zipcode = leadData.ZipCode,
                    PhoneNumber = leadData.PhoneNumber,
                    Photo = _cloudService.GetAWSFile(leadData.Photo),                    
                    CreatedBy = leadData.CreatedBy,
                    CreatedDate = leadData.CreatedDate,
                    ReferralSources = ResourceTypeNameList.Select(s => new LeadReferralSource
                    {
                        ReferType = EnumHelper.GetEnumValue<ReferralType>(s.ReferType).ToString(),
                        ReferrerValues = s.ReferData.Select(x => new ReferrerValue
                        {
                            ReferValue = EnumHelper.GetEnumDescription(EnumHelper.GetEnumValue<ReferralValue>(x.ReferValue)),
                            Description = x.Description
                        }).ToList()
                    }).ToList()
                };


                gameUserLeads.Add(lead);

            }

            gameLeadResponse.GameUserLead = gameUserLeads;

            return gameLeadResponse;
        }

        private GameUserLead MapGameLead(GameLead gameLead)
        {
            var ResourceTypeNameList = gameLead.GameLeadReferralSources.GroupBy(r => r.ReferType).Select(g => new { ReferType = g.Key, ReferData = g });

            return new GameUserLead
            {
                ID = gameLead.Id,
                GameId = gameLead.GameId,
                ReferUser = gameLead.ReferUser.FirstName + " " + gameLead.ReferUser.LastName,
                FirstName = gameLead.FirstName,
                LastName = gameLead.LastName,
                Email = gameLead.Email,
                Address = gameLead.Address,
                City = gameLead.City,
                State = gameLead.State,
                Zipcode = gameLead.ZipCode,
                PhoneNumber = gameLead.PhoneNumber,
                Photo = _cloudService.GetAWSFile(gameLead.Photo),                
                CreatedBy = gameLead.CreatedBy,
                CreatedDate = gameLead.CreatedDate,
                ReferralSources = ResourceTypeNameList== null ? null : ResourceTypeNameList.Select(s => new LeadReferralSource
                {
                    ReferType = EnumHelper.GetEnumValue<ReferralType>(s.ReferType).ToString(),
                    ReferrerValues = s.ReferData.Select(x => new ReferrerValue
                    {
                        ReferValue = EnumHelper.GetEnumDescription(EnumHelper.GetEnumValue<ReferralValue>(x.ReferValue)),
                        Description = x.Description
                    }).ToList()
                }).ToList()
            };
        }

        private void GetReferrerSource(ICollection<GameLeadReferralSources> referralSources)
        {
            var Internet = referralSources.Where(x => x.ReferType == referralSources.FirstOrDefault().ReferType);
        }


        private ICollection<GameLeadReferralSources> MapReferralSource(List<ReferralSource> referralSources)
        {
            if (referralSources == null) return new List<GameLeadReferralSources> { };
            return referralSources.Select(s => new GameLeadReferralSources
            {
                ReferType = s.ReferType,
                ReferValue = s.ReferValue,
                Description = s.Description
            }).ToList();
        }
        #endregion

    }
}
