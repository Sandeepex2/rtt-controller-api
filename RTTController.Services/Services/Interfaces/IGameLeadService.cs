﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Services.Services.Interfaces
{
    public interface IGameLeadService
    {
        GameUserLead CreateGameLead(GameLeadRequest gameLead);

        GameLeadResponse GetGameLeads();

        GameLeadResponse GetGameLeadByUserId(int userId);

        GameLeadResponse GetGameLeadByGameId(int gameId);

        GameLeadResponse GetGameLead(int gameId, int userId);
    }
}
