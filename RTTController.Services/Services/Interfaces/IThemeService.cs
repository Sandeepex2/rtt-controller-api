﻿using RTTController.Contracts.Responses;
using RTTController.Contracts.Requests;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Services.Services.Interfaces
{
    public interface IThemeService
    {
        IEnumerable<ThemeResponse> GetByGameId(int Id);

        IEnumerable<ThemeResponse> Get();

        ThemeResponse Create(ThemeRequest theme);

        ThemeResponse Update(ThemeRequest theme);

        bool Delete(int Id);
    }
    
}
