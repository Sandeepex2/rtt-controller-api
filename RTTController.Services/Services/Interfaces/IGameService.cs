﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static RTTController.Contracts.Responses.GamePlanResponse;

namespace RTTController.Services.Services.Interfaces
{
    public interface IGameService
    {
        GamePlanResponse Get(int userId);

        IEnumerable<GamePlanResponse> Get();

        bool DeleteGame(int id);

        GamePlanResponse UpdatePlan(PlanRequest request);        

        bool DeleteSubcategory(int id);

        bool SequenceUpdate(SubCategorySequecneUpdateRequest request);

        List<SettingsList> LoadSettings();

        GamePlanResponse CreateGame(GamePlanRequest game);

        bool UpdateValues(ValueRequest request);

        BaseApiResponse UpdateGame(GamePlanRequest game);

        Task<bool> DeleteGameCategoryEvent(int gameId, int eventId);

        bool UpdateGameFloorImage(UpdateGameFloorImageRequest request);
        bool AddGameFloorImage(AddGameFloorImageRequest request);

        bool DeleteGameFloor(int id);

        List<SettingsList> AddSettings(SettingsRequest request);

        SettingsList UpdateSettings(SettingsRequest request);

        PrePopulateSubCategory CreatePrePopulateSubCategory(PrePopulateSubCategory game);

        PrePopulateSubCategory UpdatePrePopulateSubCategory(PrePopulateSubCategory game);

        PrePopulateSubCategory GetPrePopulateSubCategory(int id);

        List<PrePopulateSubCategory> ListPrePopulateSubCategory();

    }
}
