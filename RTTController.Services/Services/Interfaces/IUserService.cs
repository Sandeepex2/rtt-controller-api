﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RTTController.Services.Services.Interfaces
{
    public interface IUserService
    {
        UserResponse UpdateDeviceToken(DeviceTokenRequest request);
        AuthenticateResponse Authenticate(AuthenticateRequest request);

        UserResponse GetUserById(int userId);

        UserResponse GetUserByEmail(string email);

        Task<bool> InviteUser(InviteGameRequest request);

        IEnumerable<UserResponse> GetAllUsers();

        Task<UserResponse> CreateUser(UserRequest user);                

        bool InviteUserForGame(InviteUserRequest request);

        InviteUserResponse VerifyEmailToken(string emailToken);

        bool IsGameAlreadyAssignedToUser(string email, int gameId);

        bool DeleteUserGame(DeleteUserGameRequest request);
        List<InviteGameUserResponse> GetInviteUserList();
    }
}
