﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Services.Services.Interfaces
{
    public interface IBuilderService
    {
        BuilderResponse GetBuilderById(int Id);

        IEnumerable<BuilderResponse> GetAllBuilder();

        BuilderResponse CreateBuilder(BuilderRequest builder);

        BuilderResponse UpdateBuilder(BuilderRequest builder);

        bool DeleteBuilder(int Id);
    }
}
