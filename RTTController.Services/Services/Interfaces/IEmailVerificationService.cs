﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RTTController.Services.Services.Interfaces
{
    public interface IEmailVerificationService
    {
        bool IsExists(string email);
        Task<bool> Request(string email);
        bool Reset(ResetPasswordRequest request);
        dynamic Change(ChangePasswordRequest request);

        ResetResponse VerifyResetToken(string token);
    }
}
