﻿using Microsoft.AspNetCore.Http;
using RTTController.Contracts;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RTTController.Services.Services.Interfaces
{
    public interface ICloudService
    {
        Task<UploadPhotoResponse> UploadFile(IFormFile file);

        string GetAWSFile(string keyName);

        Task<bool> SendMail(EmailRequest request);

        Task<bool> DeleteAWSFile(string keyName);

        bool IsUserExists(string email);

        bool IsUserExists(int id);

        bool IsGameExists(int id);

        bool IsGameExists(string name);

        bool IsGameExists(string name, int builderId);

        bool IsBuilderExists(int id, string name);
        bool IsBuilderExists(string name);

        bool IsBuilderExists(int id);

        bool IsCategoryExists(string name);

        bool IsCategoryExists(int id);

        bool IsSubCategoryExists(int id, string name);

        bool IsSubCategoryExists(string name);

        bool IsSubCategoryExists(int id);

        bool IsSubCategoryExistsInGame(string name, int categoryId, int gameId, int? themeId);

        bool IsEventCodeExistsInSubCategory(int gameId, int categoryId, int subcategoryId, string eventCode , int? themeId);

        int GetGameId(int id, RequestType type);

        bool UpdateGameVersion(int gameId);
    }
}
