﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Services.Services.Interfaces
{
    public interface ISubCategoryService
    {
        SubCategoryResponse Update(SubCategoryUpdateRequest request);

        SubCategoryResponse Create(SubCategoryRequest request);

        IEnumerable<SubCategoryResponse> Get();

        SubCategoryResponse GetById(int id);

    }
}
