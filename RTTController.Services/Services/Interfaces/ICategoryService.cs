﻿using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Services.Services.Interfaces
{
    public interface ICategoryService
    {
        CategoryResponse Create(CategoryRequest request);

        IEnumerable<CategoryResponse> Get();

        CategoryResponse GetCategory(int catId);

    }
}
