﻿using log4net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Entities.Models;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Helpers;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTTController.Services.Services
{
    public class EmailVerificationService : IEmailVerificationService
    {
        #region private members
        private readonly UserUnit _userUnit;
        private ILog _logger = LogManager.GetLogger(typeof(EmailVerificationService));
        private readonly ICloudService _cloudService;
        private readonly AppSettings _appSettings;
        private readonly IHostingEnvironment _host;
        #endregion
        #region Constructor
        /// <summary>
        /// Construct EmailVerification Service object
        /// </summary>
        /// <param name="userUnit"></param>
        /// <param name="cloudService"></param>
        /// <param name="appSettings"></param>
        public EmailVerificationService(UserUnit userUnit, ICloudService cloudService, IOptions<AppSettings> appSettings, IHostingEnvironment host)
        {
            _userUnit = userUnit ?? throw new ArgumentNullException("UserUnit reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _appSettings = appSettings.Value;
            _host = host;
        }

        #endregion

        #region Service Methods
        public dynamic Change(ChangePasswordRequest request)
        {
            string passwordHash, passwordSalt;

            var entity = _userUnit.User.Find(e => e.Email.ToLower() == request.Email.ToLower()).FirstOrDefault();

            if (entity == null)
                return "Email does not exists";

            if (!Helper.VerifyPassword(request.CurrentPassword, entity.Password, entity.PasswordSalt))
                return "Incorrect current password, Please enter valid password";

            Helper.GenerateSaltedHash(request.NewPassword, out passwordHash, out passwordSalt);

            entity.Password = passwordHash;
            entity.PasswordSalt = passwordSalt;

            _userUnit.User.Update(entity);
            _userUnit.SaveChanges();

            return true;
        }

        public bool IsExists(string email)
        {
            return _cloudService.IsUserExists(email);
        }

        public async Task<bool> Request(string email)
        {
            var token = Guid.NewGuid().ToString();

            UserEmailVerification userEmailVerification = new UserEmailVerification
            {
                Email = email,
                Token = token,
                IsVerified =0,
                CreateDate = DateTime.UtcNow
            };

            var response = _userUnit.UserEmailVerification.Add(userEmailVerification);
            _userUnit.SaveChanges();

            if (response != null)
            {               

                var path = _host.ContentRootPath;

                var emailTemplate = System.IO.File.ReadAllText(path + "/EmailTemplate/ResetPasswordEmailTemplate.html");
                if (!string.IsNullOrEmpty(emailTemplate))
                {
                    emailTemplate = emailTemplate.Replace("{Link}", _appSettings.BaseUrl + "/reset/" + token);
                    emailTemplate = emailTemplate.Replace("{baseUrl}", _appSettings.TemplateUrl);

                    EmailRequest request = new EmailRequest
                    {
                        Recipient = email,
                        Sender = _appSettings.SenderAddress,
                        HtmlBody = emailTemplate,
                        Subject = "Reset Password Email"
                    };

                    /* Call AWS SES Service for email */
                    var awsResponse = await _cloudService.SendMail(request);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Reset(ResetPasswordRequest request)
        {
            string passwordHash, passwordSalt;

            if (_userUnit.UserEmailVerification.Find(e => e.Email.ToLower() == request.Email.ToLower()).Count() > 0)
            {
                var entity = _userUnit.User.Find(e => e.Email.ToLower() == request.Email.ToLower()).FirstOrDefault();
                Helper.GenerateSaltedHash(request.NewPassword, out passwordHash, out passwordSalt);

                entity.Password = passwordHash;
                entity.PasswordSalt = passwordSalt;

                _userUnit.User.Update(entity);
                _userUnit.SaveChanges();

                return true;
            }
            return false;
        }

        public ResetResponse VerifyResetToken(string token)
        {
            ResetResponse response = null;
            var entity = _userUnit.UserEmailVerification.Find(e => e.Token.ToLower() == token.ToLower()).FirstOrDefault();
            if (entity != null)
            {
                entity.IsVerified = 1;

                 var emailVerification= _userUnit.UserEmailVerification.Update(entity);
                _userUnit.SaveChanges();

                return response = new ResetResponse
                {
                    Email = emailVerification.Email,
                    IsVerified =Convert.ToBoolean(emailVerification.IsVerified)
                };
            }
            else
            {
                return response;
            }
        }

        #endregion
    }
}
