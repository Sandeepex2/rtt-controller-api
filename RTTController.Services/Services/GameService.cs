﻿using RTTController.Services.Services.Interfaces;
using log4net;
using RTTController.Repository.UnitOfWork;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using System.Collections.Generic;
using System;
using RTTController.Entities.Models;
using System.Linq;
using RTTController.Services.Helpers;
using RTTController.Entities.Enums;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Globalization;

namespace RTTController.Services.Services
{
    public class GameService : IGameService
    {
        #region Private Members
        private IThemeService _themeService;
        private readonly GamePlanUnit _gamePlanUnit;
        private readonly SharedUnit _sharedUnit;
        private readonly ICloudService _cloudService;
        private readonly ISubCategoryService _subCategoryService;
        private readonly ILog _logger = LogManager.GetLogger(typeof(GameService));
        #endregion

        #region Constructor
        public GameService(IThemeService themeService, GamePlanUnit gamePlanUnit, SharedUnit sharedUnit, ICloudService cloudService, ISubCategoryService subCategoryService)
        {
            _themeService = themeService ?? throw new ArgumentNullException("ThemeService reference cannot be passed null");
            _gamePlanUnit = gamePlanUnit ?? throw new ArgumentNullException("GamePlanUnit reference cannot be passed null");
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
            _subCategoryService = subCategoryService ?? throw new ArgumentNullException("subCategoryService reference cannot be passed null");
        }
        #endregion

        #region Service Methods

        /// <summary>
        ///  Create Game Plan
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public GamePlanResponse CreateGame(GamePlanRequest request)
        {
            SubCategoryRequest subCateRequest;
            Builder builderEntity = _sharedUnit.Builder.Find(request.BuilderId);
            GamePlanResponse response = null;

            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;

            GamePlan gameEntity = new GamePlan
            {
                Builder = builderEntity,
                //BuilderId = request.BuilderId,
                Name = textInfo.ToTitleCase(request.GameName.Trim().ToLower()),
                GamePlanImage = MapGamePlanImages(request.GamePlanImages),
                BdxcommunityId = request.BDXCommunityId,
                CreatedBy = "RTTController.API",
                CreatedDate = DateTime.UtcNow,
                GameVersion = 1
            };

            var gameData = _gamePlanUnit.GamePlan.Add(gameEntity);
            _gamePlanUnit.SaveChanges();

            //Deault Theme for Material
            _themeService.Create(new ThemeRequest() { Name = "Theme1", GameId = gameData.Id, CategoryId = 7 });

            // Pre-Populate Subcategory on Game creation
            var subCategories = _gamePlanUnit.PrePopulateSubCategory.Find(o => o.Id > 0);
            foreach (var item in subCategories)
            {
                subCateRequest = new SubCategoryRequest()
                {
                    GameId = gameData.Id,
                    CategoryId = item.CategoryId,
                    Name = item.SubCategoryName,
                    Sequence = 0
                };

                _subCategoryService.Create(subCateRequest);

                // Pre-Populate values as "free" (Setting Options) for SubCategory [manage features]
                if (item.SubCategoryName.ToLower() == "manage features")
                {
                    var options = _gamePlanUnit.SettingOptions.Find(o => o.Settings.Name.ToLower() == item.SubCategoryName.ToLower());
                    foreach (var oitem in options)
                    {
                        UpdateValues(new ValueRequest() {
                            GameId = gameData.Id,
                            CategoryId = item.CategoryId,
                            SubCategory = item.SubCategoryName,
                            EventCode = oitem.Name,
                            LayerName ="free"
                        });
                    }
                }
            }

            gameData = _gamePlanUnit.GamePlan.Find(gameData.Id);

            response = GamePlanMapper(gameData);
            return response;
        }

        /// <summary>
        /// Update Game Plan
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public BaseApiResponse UpdateGame(GamePlanRequest request)
        {
            BaseApiResponse response = null;
            GamePlanResponse gamePlanResponse = null;

            var gamePlan = _gamePlanUnit.GamePlan.Find(request.GameId);

            if (gamePlan == null)
            {
                return response = new BaseApiResponse
                {
                    StatusCode = StatusCodes.Status404NotFound,
                    Message = $"No game plan record found for game id {request.GameId}"
                };
            }


            Category category = _sharedUnit.Category.Find(request.CategoryGameMap.CategoryId);

            if (category == null)
            {
                return response = new BaseApiResponse
                {
                    StatusCode = StatusCodes.Status404NotFound,
                    Message = $"No Category  found for category id {request.CategoryGameMap.CategoryId}"
                };
            }

            Theme theme = _sharedUnit.Theme.Find(request.CategoryGameMap.ThemeId);


            SubCategory subCategory = _sharedUnit.SubCategory.Find(request.CategoryGameMap.SubCategoryId);

            if (subCategory == null)
            {
                return response = new BaseApiResponse
                {
                    StatusCode = StatusCodes.Status404NotFound,
                    Message = $"No SubCategory found for subcategory id {request.CategoryGameMap.SubCategoryId}"
                };
            }
           

            gamePlan.GameVersion = gamePlan.GameVersion + 1;

            CategoryGameMap categoryGameMap = null;
            

            categoryGameMap = _gamePlanUnit.CategoryGameMap.Find(x => x.CategoryId == category.Id && x.GameId == gamePlan.Id && x.SubCatId == subCategory.Id && Convert.ToInt32(x.ThemeId) ==Convert.ToInt32(request.CategoryGameMap.ThemeId)).FirstOrDefault();

            if (categoryGameMap == null)
            {               
                gamePlan.CategoryGameMap.Add(new CategoryGameMap
                {
                    Game = gamePlan,
                    Category = category,                    
                    Theme =theme,
                    SubCat = subCategory,
                    GamePlanCategoryValue = request.CategoryGameMap.GamePlanCategoryEvents?.Select(x => new GamePlanCategoryValue
                    {
                        EventCode = x.EventCode,
                        ImageUrl = x.ImageUrl,
                        LayerName = x.LayerName
                    }).ToList()
                });

                var gameData = _gamePlanUnit.GamePlan.Update(gamePlan);
                _gamePlanUnit.SaveChanges();

                gamePlanResponse = GamePlanMapper(gameData);
            }
            else
            {                
                GamePlanCategoryValue gamePlanCategoryValue = null;
                foreach (var events in request.CategoryGameMap.GamePlanCategoryEvents)
                {
                    gamePlanCategoryValue = new GamePlanCategoryValue();
                    gamePlanCategoryValue.GameCategoryMapId = categoryGameMap.Id;
                    gamePlanCategoryValue.EventCode = events.EventCode;
                    gamePlanCategoryValue.ImageUrl = events.ImageUrl;
                    gamePlanCategoryValue.LayerName = events.LayerName;

                    using (var transaction = _gamePlanUnit.BeginTransaction())
                    {
                        try
                        {
                            var gameEvent = _gamePlanUnit.GamePlanCategoryValue.Add(gamePlanCategoryValue);
                            _gamePlanUnit.GamePlan.Update(gamePlan);

                            _gamePlanUnit.SaveChanges();

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.Message);
                            transaction.Rollback();
                            throw ex;
                        }
                    }

                }

                gamePlanResponse = Get(gamePlan.Id);
            }

            return response = new BaseApiResponse
            {
                StatusCode = StatusCodes.Status200OK,
                Result = gamePlanResponse
            };

        }

        /// <summary>
        /// Delete game category event
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteGameCategoryEvent(int gameId, int eventId)
        {
            var game = _gamePlanUnit.GamePlan.Find(gameId);

            if (game != null)
            {
                var _entity = _gamePlanUnit.GamePlanCategoryValue.Find(eventId);

                //Remove Object from AWS s3

                var fileResponse = await _cloudService.DeleteAWSFile(_entity.ImageUrl);
                if (fileResponse)
                {
                    var result = _gamePlanUnit.GamePlanCategoryValue.Delete(_entity.Id);
                    _gamePlanUnit.SaveChanges();

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Update GamePlan floor Image
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool UpdateGameFloorImage(UpdateGameFloorImageRequest request)
        {
            var gamePlanImage = _gamePlanUnit.GamePlanImage.Find(x => x.GameId == request.GameID && x.Id == request.GameFloorID).FirstOrDefault();

            if (gamePlanImage != null)
            {
                gamePlanImage.Game.GameVersion = gamePlanImage.Game.GameVersion + 1; //Game version update

                gamePlanImage.ImageUrl = request.ImageName;
                _gamePlanUnit.GamePlanImage.Update(gamePlanImage);
                _gamePlanUnit.SaveChanges();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Add GamePlan floor Image
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool AddGameFloorImage(AddGameFloorImageRequest request)
        {
            var gamePlan = _gamePlanUnit.GamePlan.Find(request.GameID);

            gamePlan.GameVersion = gamePlan.GameVersion + 1;

            foreach (var item in request.GamePlanImages)
                gamePlan.GamePlanImage.Add(new GamePlanImage()
                {
                    FloorType = item.FloorId,
                    ImageUrl = item.FloorImage,
                    GameId = request.GameID
                });

            _gamePlanUnit.GamePlan.Update(gamePlan);
            _gamePlanUnit.SaveChanges();

            return true;
        }


        /// <summary>
        /// Get all game plans
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GamePlanResponse> Get()
        {
            var games = _gamePlanUnit.GamePlan.Find(x => x.Id != 0).ToList();
            return GetAllGames(games);
        }

        /// <summary>
        /// Retrieve game plan by Id
        /// </summary>
        /// <param name="gameId"></param>
        /// <returns></returns>
        public GamePlanResponse Get(int id)
        {
            var game = _gamePlanUnit.GamePlan.Find(id);

            return GamePlanMapper(game);
        }

        /// <summary>
        /// Loadsettings by game id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<SettingsList> LoadSettings()
        {
            var settings = _gamePlanUnit.Settings.Find(o => o.Id != 0);

            return settings.Select(s => new SettingsList
            {
                Id = s.Id,
                Name = s.Name,
                Options = s.SettingOptions.Select(o => new OptionsList() { Id = o.Id, Name = o.Name }).ToList()
            }).ToList();
        }

        public GamePlanResponse UpdatePlan(PlanRequest request)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            var game = _gamePlanUnit.GamePlan.Find(request.Id);            

            game.GameVersion = game.GameVersion + 1; //Game version update
            game.Name = textInfo.ToTitleCase(request.Name.Trim().ToLower());            

            _gamePlanUnit.GamePlan.Update(game);
            _gamePlanUnit.SaveChanges();

            return GamePlanMapper(game);
        }


        /// <summary>
        /// Delete game by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGame(int id)
        {
            var game = _gamePlanUnit.GamePlan.Delete(id);
            _gamePlanUnit.SaveChanges();

            return true;
        }


        /// <summary>
        /// Delete subcategory by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteSubcategory(int id)
        {
            var game = _gamePlanUnit.CategoryGameMap.Delete(id);
            _gamePlanUnit.SaveChanges();

            return true;
        }


        /// <summary>
        /// Delete GameFloor by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteGameFloor(int id)
        {
            var game = _gamePlanUnit.GamePlanImage.Delete(id);
            _gamePlanUnit.SaveChanges();

            return true;
        }
        #endregion

        #region Private Methods
        private GamePlanResponse GamePlanMapper(GamePlan planData)
        {
            if (planData == null) return null;
            var categories = _sharedUnit.Category.Find(x => x.Id != 0).ToList();           

            return new GamePlanResponse
            {
                GameId = planData.Id,
                GameName = planData.Name,
                GameVersion = planData.GameVersion,
                BDXPartnerId = planData.Builder.BdxpartnerId,
                BuilderId = planData.BuilderId,
                BuilderName = planData.Builder.Name,
                BuilderImage = planData.Builder.LogoUrl,
                BDXCommunityId = planData.BdxcommunityId,

                GamePlanImages = planData.GamePlanImage?.Select(g => new GamePlanResponse.GamePlanImage
                {
                    FloorName = EnumHelper.GetEnumValue<FloorType>(g.FloorType).ToString(),
                    FloorImage = _cloudService.GetAWSFile(g.ImageUrl)
                }).ToList(),
                Categories = categories.Select(s => new GamePlanResponse.GameCategory
                {
                    CategoryId = s.Id,
                    CategoryName = s.Name,
                    EventCode = s.EventCode,
                    SubCategories = planData.CategoryGameMap.Where(x => x.CategoryId == s.Id && x.ThemeId == null)
                    .Select(a => new GamePlanResponse.GameSubCategory
                    {
                        Id = a.Id,
                        SubCategoryId = a.SubCatId ?? 0,
                        Name = a.SubCat.Name,
                        Sequence = a.Sequence ?? 0,
                        EventObject = a.GamePlanCategoryValue.Select(x => new GamePlanResponse.EventValueObject
                        {
                            EventId = x.Id,
                            EventCode = x.EventCode,
                            EventImageUrl = _cloudService.GetAWSFile(x.ImageUrl),
                            LayerName = x.LayerName
                        }).ToList()
                    }).OrderBy(o => o.Sequence).ToList(),
                    Themes = planData.Theme.Where(tc => tc.CategoryId == s.Id)
                    .Select(t => new GamePlanResponse.GameTheme
                    {
                        Id = t.Id,
                        Name = t.Name,
                        GameId = planData.Id,
                        SubCategories = planData.CategoryGameMap.Where(x => x.CategoryId == s.Id && x.ThemeId == t.Id)
                        .Select(a => new GamePlanResponse.GameSubCategory
                        {
                            Id = a.Id,
                            SubCategoryId = a.SubCatId ?? 0,
                            Name = a.SubCat.Name,
                            Sequence = a.Sequence ?? 0,
                            EventObject = a.GamePlanCategoryValue.Select(x => new GamePlanResponse.EventValueObject
                            {
                                EventId = x.Id,
                                EventCode = x.EventCode,
                                EventImageUrl = _cloudService.GetAWSFile(x.ImageUrl),
                                LayerName = x.LayerName
                            }).ToList()
                        }).OrderBy(o => o.Sequence).ToList()
                    }).ToList()
                }).ToList(),
                GamePlanLeads = planData.GameLead == null ? null : planData.GameLead.Select(p => new GamePlanResponse.GamePlanLead
                {
                    ReferUser = p.ReferUser.FirstName + " " + p.ReferUser.LastName,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    Email = p.Email,
                    Address = p.Address,
                    City = p.City,
                    State = p.State,
                    ZipCode = p.ZipCode
                }).ToList()
            };
        }

        private List<GamePlanResponse> GetAllGames(List<GamePlan> gamePlans)
        {
            if (gamePlans.Count == 0) return new List<GamePlanResponse>();

            List<GamePlanResponse> gamePlanResources = new List<GamePlanResponse>();
            GamePlanResponse gamePlanResource = null;

            foreach (var planData in gamePlans)
            {
                var categ = planData.CategoryGameMap.GroupBy(x => x.Category.Name).Select(s => new { CategoyName = s.Key, CategoryGameMap = s });
                gamePlanResource = new GamePlanResponse
                {
                    GameId = planData.Id,
                    GameName = planData.Name,
                    GameVersion = planData.GameVersion,
                    BuilderId = planData.BuilderId,
                    BuilderName = planData.Builder.Name,
                    BuilderImage = planData.Builder.LogoUrl,
                    GamePlanImages = planData.GamePlanImage?.Select(g => new GamePlanResponse.GamePlanImage
                    {
                        FloorName = EnumHelper.GetEnumValue<FloorType>(g.FloorType).ToString(),
                        FloorImage = _cloudService.GetAWSFile(g.ImageUrl)
                    }).ToList(),
                    Categories = categ.Select(s => new GamePlanResponse.GameCategory
                    {
                        CategoryId = s.CategoryGameMap.FirstOrDefault().Category.Id,
                        CategoryName = s.CategoyName,
                        EventCode = s.CategoryGameMap.FirstOrDefault().Category.EventCode,
                        SubCategories = s.CategoryGameMap.Select(a => new GamePlanResponse.GameSubCategory
                        {
                            SubCategoryId = a.SubCatId ?? 0,
                            Name = a.SubCat.Name,
                            Sequence = a.Sequence ?? 0,
                            EventObject = a.GamePlanCategoryValue.Select(x => new GamePlanResponse.EventValueObject
                            {
                                EventCode = x.EventCode,
                                EventImageUrl = x.ImageUrl
                            }).ToList()
                        }).ToList()
                    }).ToList(),
                    GamePlanLeads = planData.GameLead == null ? null : planData.GameLead.Select(p => new GamePlanResponse.GamePlanLead
                    {

                        ReferUser = p.ReferUser.FirstName + " " + p.ReferUser.LastName,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        Email = p.Email,
                        Address = p.Address,
                        City = p.City,
                        State = p.State,
                        ZipCode = p.ZipCode,

                    }).ToList()
                };

                gamePlanResources.Add(gamePlanResource);
            }

            return gamePlanResources;
        }

        private ICollection<GamePlanImage> MapGamePlanImages(List<GameFloorImage> gameImages)
        {
            return gameImages.Select(s => new GamePlanImage
            {
                FloorType = s.FloorId,
                ImageUrl = s.FloorImage
            }).ToList();
        }

        public List<SettingsList> AddSettings(SettingsRequest request)
        {
            List<Entities.Models.SettingOptions> options = new List<Entities.Models.SettingOptions>();
            foreach (var item in request.Options)
            {
                options.Add(new Entities.Models.SettingOptions()
                {
                    Name = item.Name,
                    SettingsId = request.Id
                });
            }

            var entity = _gamePlanUnit.Settings.Add(new Settings()
            {
                Name = request.Name,
                SettingOptions = options
            });

            _gamePlanUnit.SaveChanges();

            return LoadSettings();
        }

        public SettingsList UpdateSettings(SettingsRequest request)
        {
            List<Entities.Models.SettingOptions> options = new List<Entities.Models.SettingOptions>();
            foreach (var item in request.Options)
            {
                options.Add(new Entities.Models.SettingOptions()
                {
                    Name = item.Name,
                    SettingsId = request.Id
                });
            }

            var entity = _gamePlanUnit.Settings.Find(request.Id);
            entity.Name = request.Name;
            entity.SettingOptions = options;


            var updatedEntity = _gamePlanUnit.Settings.Update(entity);
            _gamePlanUnit.SaveChanges();

            return new SettingsList()
            {
                Id = updatedEntity.Id,
                Name = updatedEntity.Name
            };
        }

        public bool UpdateValues(ValueRequest request)
        {
            int mapId;
            _logger.Info("Update Value Request:" + JsonConvert.SerializeObject(request));

            var category = _sharedUnit.Category.Find(request.CategoryId);
            SubCategory subCategory = _sharedUnit.SubCategory.Find(o => o.Name == request.SubCategory).FirstOrDefault();

            if (subCategory == null)
            {
                subCategory = new SubCategory
                {
                    Name = request.SubCategory.Trim()
                };

                CategorySubCategoryMapping categorySubCategory = new CategorySubCategoryMapping
                {
                    Cat = category,
                    SubCat = subCategory
                };

                subCategory.CategorySubCategoryMapping.Add(categorySubCategory);

                CategoryGameMap categoryGameMap = new CategoryGameMap
                {
                    Category = category,
                    SubCat = subCategory,
                    GameId = request.GameId,
                    Sequence = _sharedUnit.CategoryGameMap
                        .Find(x => x.GameId == request.GameId && x.CategoryId == request.CategoryId)
                        .Max(m => m.Sequence) + 1
                };
                subCategory.CategoryGameMap.Add(categoryGameMap);


                var data = _sharedUnit.SubCategory.Add(subCategory);
                _sharedUnit.SaveChanges();

                mapId = data.CategoryGameMap.Where(c => c.GameId == request.GameId).Max(x => x.Id);
                _logger.Info("Subcategory" + request.SubCategory + " CategoryGameMap Done:" + mapId);
            }
            else
            {
                var categoryGameMap = _sharedUnit.CategoryGameMap.Find(m => m.SubCatId == subCategory.Id && m.GameId == request.GameId).FirstOrDefault();

                if (categoryGameMap == null)
                {
                    categoryGameMap = new CategoryGameMap
                    {
                        Category = category,
                        SubCat = subCategory,
                        GameId = request.GameId,
                        Sequence = _sharedUnit.CategoryGameMap
                        .Find(x => x.GameId == request.GameId && x.CategoryId == request.CategoryId)
                        .Max(m => m.Sequence) + 1
                    };

                    subCategory.CategoryGameMap.Add(categoryGameMap);

                    var data = _sharedUnit.SubCategory.Update(subCategory);
                    _sharedUnit.SaveChanges();

                    mapId = data.CategoryGameMap.Where(c => c.GameId == request.GameId).Max(x => x.Id);
                    _logger.Info("CategoryGameMap Done:" + mapId);
                }
                else
                {
                    mapId = categoryGameMap.Id;
                    _logger.Info("CategoryGameMap exists:" + mapId);
                }
            }

            var entity = _sharedUnit.GamePlanCategoryValue.Find(gp => gp.EventCode.ToLower() == request.EventCode.ToLower() && gp.GameCategoryMapId == mapId).FirstOrDefault();
            if (entity == null)
            {
                _sharedUnit.GamePlanCategoryValue.Add(new GamePlanCategoryValue()
                {
                    GameCategoryMapId = mapId,
                    EventCode = request.EventCode,
                    LayerName = request.LayerName,
                    ImageUrl = ""
                });
            }
            else
            {
                entity.LayerName = request.LayerName;
                _sharedUnit.GamePlanCategoryValue.Update(entity);
            }

            _sharedUnit.SaveChanges();

            return true;
        }

        public bool SequenceUpdate(SubCategorySequecneUpdateRequest request)
        {
            _logger.Info("Sequence Update Request:" + JsonConvert.SerializeObject(request));
            int sequence = 0;
            using (var transaction = _sharedUnit.BeginTransaction())
            {
                try
                {
                    foreach (var item in request.sequenceArray)
                    {
                        var entity = _sharedUnit.CategoryGameMap.Find(Convert.ToInt32(item));
                        entity.Sequence = sequence ++;

                        _sharedUnit.CategoryGameMap.Update(entity);
                        _sharedUnit.SaveChanges();
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
            }
            return true;
        }

        public Contracts.Responses.PrePopulateSubCategory CreatePrePopulateSubCategory(Contracts.Responses.PrePopulateSubCategory game)
        {
            var entity = _gamePlanUnit.PrePopulateSubCategory.Add(new Entities.Models.PrePopulateSubCategory()
            {
                CategoryId = game.CategoryId,
                SubCategoryName = game.SubCategoryName
            });
            _gamePlanUnit.SaveChanges();

            return GetPrePopulateSubCategory(entity.Id);
        }

        public Contracts.Responses.PrePopulateSubCategory UpdatePrePopulateSubCategory(Contracts.Responses.PrePopulateSubCategory game)
        {
            var entity = _gamePlanUnit.PrePopulateSubCategory.Find(game.Id);
            if (entity != null)
            {
                entity.SubCategoryName = game.SubCategoryName;
                _gamePlanUnit.PrePopulateSubCategory.Update(entity);
                _gamePlanUnit.SaveChanges();
            }

            return GetPrePopulateSubCategory(entity.Id);
        }

        public Contracts.Responses.PrePopulateSubCategory GetPrePopulateSubCategory(int id)
        {
            var entity = _gamePlanUnit.PrePopulateSubCategory.Find(id);

            return new Contracts.Responses.PrePopulateSubCategory()
            {
                Id = entity.Id,
                CategoryId = entity.CategoryId,
                SubCategoryName = entity.SubCategoryName
            };
        }

        public List<Contracts.Responses.PrePopulateSubCategory> ListPrePopulateSubCategory()
        {
            var entity = _gamePlanUnit.PrePopulateSubCategory.Find(o => o.Id > 0);

            return entity.Select(e => new Contracts.Responses.PrePopulateSubCategory()
            {
                Id = e.Id,
                CategoryId = e.CategoryId,
                SubCategoryName = e.SubCategoryName
            }).ToList();
        }
        #endregion
    }
}
