﻿using log4net;
using RTTController.Contracts.Requests;
using RTTController.Contracts.Responses;
using RTTController.Repository.UnitOfWork;
using RTTController.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using RTTController.Services.Helpers;
using RTTController.Entities.Enums;
using RTTController.Entities.Models;

namespace RTTController.Services.Services
{
    public class BuilderService : IBuilderService
    {
        #region Private Members
        private readonly ILog _logger = LogManager.GetLogger(typeof(BuilderService));
        private readonly SharedUnit _sharedUnit;
        private readonly ICloudService _cloudService;
        #endregion

        #region Constructor
        public BuilderService(ICloudService cloudService , SharedUnit sharedUnit)
        {
            _sharedUnit = sharedUnit ?? throw new ArgumentNullException("SharedUnit reference cannot be passed null");
            _cloudService = cloudService ?? throw new ArgumentNullException("CloudService reference cannot be passed null");
        }
        #endregion

        #region Service Methods
        public BuilderResponse CreateBuilder(BuilderRequest builder)
        {
            BuilderResponse response = null;            

            Builder builderModel = new Builder
            {             
                Name = builder.Name.Trim(),
                IsActive = 1,
                LogoUrl = builder.LogoUrl,
                BdxpartnerId = builder.BdxpartnerId
            };
            

            var builderData = _sharedUnit.Builder.Add(builderModel);

            _sharedUnit.SaveChanges();

            response = BuilderMapper(builderData);

            return response;
        }       

        public BuilderResponse UpdateBuilder(BuilderRequest builder)
        {
            var entity = _sharedUnit.Builder.Find(builder.Id);

            entity.Name = builder.Name;
            entity.LogoUrl = builder.LogoUrl == "" ? entity.LogoUrl : builder.LogoUrl;

            var builderData = _sharedUnit.Builder.Update(entity);
            _sharedUnit.SaveChanges();

            BuilderResponse response = BuilderMapper(builderData);
            return response;            
        }

        public bool DeleteBuilder(int Id)
        {            
            _sharedUnit.Builder.Delete(Id);
            _sharedUnit.SaveChanges();
            return true;            
        }

        public IEnumerable<BuilderResponse> GetAllBuilder()
        {
            var builders = _sharedUnit.Builder.Find(X => X.Id != 0).ToList();
            return GetBuilders(builders);
        }

        public BuilderResponse GetBuilderById(int Id)
        {
            var builder = _sharedUnit.Builder.Find(Id);
            return BuilderMapper(builder);
        }
        #endregion

        #region Builder Mapper Methods

        public BuilderResponse BuilderMapper(Builder builder)
        {
            return new BuilderResponse
            {
                BuilderId = builder.Id,
                BuilderName = builder.Name,
                IsActive =Convert.ToBoolean(builder.IsActive),
                LogoUrl = _cloudService.GetAWSFile(builder.LogoUrl),
                BDXPartnerId = builder.BdxpartnerId,
                GamePlans = builder.GamePlan == null ? null : builder.GamePlan.Select(planData => new GamePlanResponse
                {
                    GameId = planData.Id,
                    GameName = planData.Name,
                    BuilderId = planData.BuilderId,
                    BuilderName = planData.Builder.Name,
                    GamePlanImages = planData.GamePlanImage?.Select(g => new GamePlanResponse.GamePlanImage
                    {
                        FloorId = g.Id,
                        FloorName = EnumHelper.GetEnumValue<FloorType>(g.FloorType).ToString(),
                        FloorImage =_cloudService.GetAWSFile(g.ImageUrl)
                    }).ToList(),
                    GamePlanLeads = planData.GameLead == null ? null : planData.GameLead.Select(p => new GamePlanResponse.GamePlanLead
                    {
                        ReferUser = p.ReferUser.FirstName + " " + p.ReferUser.LastName,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        Email = p.Email,
                        Address = p.Address,
                        City = p.City,
                        State = p.State,
                        ZipCode = p.ZipCode
                    }).ToList()
                }).ToList()
            };
        }

        private IEnumerable<BuilderResponse> GetBuilders(List<Builder> builders)
        {
            return builders.Select(builder => new BuilderResponse
            {
                BuilderId = builder.Id,
                BuilderName = builder.Name,
                IsActive = Convert.ToBoolean(builder.IsActive),
                LogoUrl =_cloudService.GetAWSFile(builder.LogoUrl),
                BDXPartnerId = builder.BdxpartnerId,
                GamePlans = builder.GamePlan == null ? null : builder.GamePlan.Select(planData => new GamePlanResponse
                {
                    GameId = planData.Id,
                    GameName = planData.Name,
                    BuilderId = planData.BuilderId,
                    BuilderName = planData.Builder.Name,
                    GamePlanImages = planData.GamePlanImage?.Select(g => new GamePlanResponse.GamePlanImage
                    {
                        FloorName = EnumHelper.GetEnumValue<FloorType>(g.FloorType).ToString(),
                        FloorImage =_cloudService.GetAWSFile(g.ImageUrl)
                    }).ToList(),
                    GamePlanLeads = planData.GameLead == null ? null : planData.GameLead.Select(p => new GamePlanResponse.GamePlanLead
                    {
                        ReferUser = p.ReferUser.FirstName + " " + p.ReferUser.LastName,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        Email = p.Email,
                        Address = p.Address,
                        City = p.City,
                        State = p.State,
                        ZipCode = p.ZipCode
                    }).ToList()
                }).ToList()
            });
        }

        #endregion
    }
}
