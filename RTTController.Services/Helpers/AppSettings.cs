﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RTTController.Services.Helpers
{
    public class AppSettings
    {
        public string AudienceSecret { get; set; }

        public string Issuer { get; set; }

        public string AudienceId { get; set; }

        public string TemplateUrl { get; set; }       

        public string SenderAddress { get; set; }

        public string BaseUrl { get; set; }

        public string AWSAccessKey { get; set; }

        public string AWSSecretKey { get; set; }

        public string AWSS3Bucket { get; set; }

        public string AWSS3ProfileBucket { get; set; }     

    }
}
